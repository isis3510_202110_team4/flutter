import 'package:flutter/cupertino.dart';
import 'package:home_services/models/user.dart';

class UserNotifier extends ChangeNotifier {
  final User _user = new User(favorites: []);

  User get user => _user;

  void setUser(User user) {
    print("USER is set ${user.toString()}");
    _user.copyWithin(user);
    notifyListeners();
  }
}
