import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/models/service_provider.dart';

class ServiceProvidersNotifier extends StateNotifier<List<ServiceProvider>> {
  ServiceProvidersNotifier({List<ServiceProvider> listOfServiceProviders})
      : super(listOfServiceProviders ?? []);

  void setServiceProviderList(List<ServiceProvider> serviceProviderList) {
    state = serviceProviderList;
  }

  void addServiceProvider(ServiceProvider serviceProvider) {
    state = [...state, serviceProvider];
  }

  void removeServiceProvider(ServiceProvider serviceProvider) {
    state =
        state.where((i) => i.id_number != serviceProvider.id_number).toList();
  }
}
