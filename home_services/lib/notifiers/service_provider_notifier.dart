import 'package:flutter/cupertino.dart';
import 'package:home_services/models/service_provider.dart';

class ServiceProviderNotifier extends ChangeNotifier {
  final ServiceProvider _serviceProvider = new ServiceProvider();

  ServiceProvider get serviceProvider => _serviceProvider;

  void setServiceProvider(ServiceProvider serviceProvider) {
    print("serviceProvider is set ${serviceProvider.toString()}");
    _serviceProvider.copyWithin(serviceProvider);
    notifyListeners();
  }
}
