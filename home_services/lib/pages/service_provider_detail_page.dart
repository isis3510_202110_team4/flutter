import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_select/smart_select.dart';

import '../controllers/user_controller.dart';
import '../models/service_provider.dart';

class ServiceProviderDetailPage extends StatefulWidget {
  final ServiceProvider serviceProvider;
  final User user;
  final SharedPreferences sharedPreferences;
  final String source;

  ServiceProviderDetailPage(
      {this.serviceProvider, this.sharedPreferences, this.user, this.source});

  @override
  _ServiceProviderDetailPageState createState() =>
      _ServiceProviderDetailPageState();
}

class _ServiceProviderDetailPageState extends State<ServiceProviderDetailPage>
    with NetworkUtils {
  bool _isFavorite = false;
  Function _showModal;
  Location _location = Location();
  String _likedFeature = "SK";
  void checkIfFavorite() {
    print("Checking if favorite");
    setState(() {
      if (widget.user.favorites != null && widget.user.favorites.length > 0) {
        _isFavorite = (widget.user.favorites
            .any((element) => element == widget.serviceProvider.id));
      } else
        _isFavorite = false;
    });
  }

  @override
  void initState() {
    super.initState();
    checkIfFavorite();
  }

  void toggleFavorite(BuildContext context) {
    NetworkUtils.checkNetworkConnectivity().then((hasConnection) {
      if (hasConnection == true) {
        if (!_isFavorite) {
          UserController.addToFavorites(widget.user.id,
              widget.serviceProvider.id, widget.serviceProvider.name);
          widget.user.favorites.add(widget.serviceProvider.id);
        }
        if (_isFavorite) {
          UserController.removeFromFavorites(widget.user.id,
              widget.serviceProvider.id, widget.serviceProvider.name);
          widget.user.favorites.remove(widget.serviceProvider.id);
        }
        setState(() {
          _isFavorite = !_isFavorite;
        });
      } else {
        createNetworkError(context, 'Change favorite service providers', () {
          Navigator.pop(context);
        });
      }
    });
  }

  List<Widget> createPreviousJobs() {
    List<Widget> listOfWidgets = [];
    if (widget.serviceProvider != null &&
        widget.serviceProvider.previousJobs != null) {
      print(
          "Loading jobs from ${widget.serviceProvider.id} -- ${widget.serviceProvider.previousJobs}");
      for (var i = 0; (i < widget.serviceProvider.previousJobs.length); i++) {
        listOfWidgets.add(Container(
            width: 160,
            child: Card(
              child: Wrap(
                children: [
                  CachedNetworkImage(
                      imageUrl: widget.serviceProvider.previousJobs[i].imageUrl,
                      placeholder: (context, url) =>
                          new CircularProgressIndicator()),
                  ListTile(
                    title:
                        Text(widget.serviceProvider.previousJobs[i].category),
                    subtitle: Text(widget.serviceProvider.previousJobs[i].desc),
                  )
                ],
              ),
            )));
      }
    }
    return listOfWidgets;
  }

  List<Widget> createSkills() {
    List<Widget> listOfWidgets = [];
    print("LOADED SKILLS ${widget.serviceProvider.skill}");
    if (widget.serviceProvider != null &&
        widget.serviceProvider.skill != null) {
      for (var i = 0; (i < widget.serviceProvider.skill.length); i++) {
        var icon;
        switch (widget.serviceProvider.skill[i]) {
          case 'Strong':
            icon = Icon(Icons.handyman);
            break;
          case 'Punctuality':
            icon = Icon(Icons.calendar_today_outlined);
            break;
          case 'Creative':
            icon = Icon(Icons.brush_outlined);
            break;
          case 'Communication':
            icon = Icon(Icons.hearing_sharp);
            break;
          case 'Effectiveness':
            icon = Icon(Icons.access_time);
            break;
          case 'Manual Dexterity':
            icon = Icon(Icons.construction);
            break;
          case 'Detail-oriented':
            icon = Icon(Icons.remove_red_eye);
            break;
          default:
            icon = Icon(Icons.add);
            break;
        }
        listOfWidgets.add(
          ListTile(
            leading: icon,
            title: Text(widget.serviceProvider.skill[i]),
            dense: true,
          ),
        );
      }
    }
    return listOfWidgets;
  }

  @override
  Widget build(BuildContext context) {
    final previousJobs = createPreviousJobs();
    final skillsList = createSkills();
    final pricePerHour = Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        "\$ " + widget.serviceProvider.price_per_hour.toString() + "/hr",
        style: TextStyle(color: Colors.white),
      ),
    );

    final topContentText = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 70.0),
        Text(
          widget.serviceProvider.name,
          style: TextStyle(color: Colors.white, fontSize: 45.0),
        ),
        SizedBox(height: 175.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                )),
            Expanded(
                flex: 6,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                )),
            Expanded(flex: 3, child: Center(child: pricePerHour))
          ],
        ),
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(left: 10.0),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: new BoxDecoration()),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              image: DecorationImage(
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.25), BlendMode.multiply),
            image: CachedNetworkImageProvider(
                widget.serviceProvider.profile_picture),
            fit: BoxFit.cover,
          )),
          child: Center(
            child: topContentText,
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        ),
      ],
    );

    final bottomContentText = Text(
      '${widget.serviceProvider.bio ?? widget.serviceProvider.name}',
      style: TextStyle(fontSize: 18.0),
    );
    final readButton = Container(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        width: MediaQuery.of(context).size.width,
        height: 95,
        child: ElevatedButton(
          onPressed: () async {
            bool hasConnection = await NetworkUtils.checkNetworkConnectivity();
            if (hasConnection) {
              LocationData currentLocation = await _location.getLocation();

              DatePicker.showDateTimePicker(context,
                  showTitleActions: true,
                  minTime: DateTime.now().add(Duration(hours: 1)),
                  onChanged: (date) {}, onConfirm: (date) async {
                await UserController.createRequest(
                    widget.serviceProvider,
                    widget.sharedPreferences,
                    widget.user,
                    widget.source,
                    date,
                    currentLocation.latitude,
                    currentLocation.longitude);
                _showModal();
              }, currentTime: DateTime.now().add(Duration(hours: 1)));
            } else {
              createNetworkError(context, 'Book an appointment', () {
                Navigator.pop(context);
              });
            }
          },
          style: ButtonStyle(
              padding:
                  MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
              foregroundColor: MaterialStateProperty.all<Color>(Colors.green),
              backgroundColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.pressed))
                    return Colors.blue;
                  return Colors.green; // Use the component's default.
                },
              ),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.green)))),
          child: Text("Book Now",
              style: TextStyle(color: Colors.white, fontSize: 18)),
        ));
    final bottomContent = Container(
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Theme.of(context).primaryColor,

      padding: EdgeInsets.all(12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          children: <Widget>[
            bottomContentText,
            SizedBox(height: 15),
            previousJobs.length > 0
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "PREVIOUS WORKS",
                      style: TextStyle(
                          fontSize: 22.0, fontWeight: FontWeight.bold),
                    ),
                  )
                : Container(),
            previousJobs.length > 0
                ? Container(
                    margin: EdgeInsets.symmetric(vertical: 20.0),
                    height: 300,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: previousJobs,
                    ))
                : SizedBox(
                    height: 20,
                  ),
            skillsList.length > 0
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "SKILLS",
                      style: TextStyle(
                          fontSize: 22.0, fontWeight: FontWeight.bold),
                    ),
                  )
                : Container(),
            skillsList.length > 0
                ? SingleChildScrollView(
                    child: Container(
                        height: skillsList.length * 50.0,
                        child: ListView(
                          children: skillsList,
                        )),
                  )
                : Container(),
            readButton,
            SmartSelect<String>.single(
              title: 'Why did you choose this service provider?',
              modalConfirm: true,
              value: _likedFeature,
              choiceType: S2ChoiceType.radios,
              onChange: (selected) {
                print("Selected $selected --- ${selected.value}");
                setState(() {
                  _likedFeature = selected.value;
                });
              },
              modalValidation: (selected) {
                if (selected == null) return 'Select at least one';
                return null;
              },
              modalType: S2ModalType.popupDialog,
              modalConfig: const S2ModalConfig(
                style: S2ModalStyle(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                ),
              ),
              modalFooterBuilder: (context, state) {
                return Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 12.0,
                    vertical: 7.0,
                  ),
                  child: Row(
                    children: <Widget>[
                      const Spacer(),
                      TextButton(
                        child: const Text('Cancel'),
                        onPressed: () => state.closeModal(confirmed: false),
                      ),
                      const SizedBox(width: 5),
                      TextButton(
                        child:
                            Text('Ok', style: TextStyle(color: Colors.white)),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                                Theme.of(context).primaryColor),
                            textStyle: MaterialStateProperty.all(
                                TextStyle(color: Colors.white))),
                        onPressed: () {
                          state.closeModal(confirmed: true);
                          EasyLoading.show(status: 'Publishing vote ...');
                          Future.delayed(Duration(seconds: 2), () {
                            print("_likedFeature $_likedFeature");
                            UserController.postSurvey(widget.serviceProvider.id,
                                widget.user.id, _likedFeature);
                          });
                        },
                      ),
                    ],
                  ),
                );
              },
              choiceItems: [
                S2Choice<String>(value: 'SK', title: 'Skills'),
                S2Choice<String>(value: 'RT', title: 'Rating'),
                S2Choice<String>(value: 'BG', title: 'Biography'),
                S2Choice<String>(value: 'PW', title: 'Previous works'),
                S2Choice<String>(
                    value: 'PR', title: 'Personal recommendations'),
              ],
              modalHeaderBuilder: (context, state) {
                return Container(
                  padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
                  child: state.modalTitle,
                );
              },
              tileBuilder: (context, state) {
                _showModal = state.showModal;
                return Container();
              },
            )
          ],
        ),
      ),
    );

    return Scaffold(
      floatingActionButton: SpeedDial(
        /// both default to 16
        marginEnd: 18,
        marginBottom: 20,
        // animatedIcon: AnimatedIcons.menu_close,
        // animatedIconTheme: IconThemeData(size: 22.0),
        /// This is ignored if animatedIcon is non null
        icon: Icons.add,
        activeIcon: Icons.remove,
        // iconTheme: IconThemeData(color: Colors.grey[50], size: 30),
        /// The label of the main button.
        // label: Text("Open Speed Dial"),
        /// The active label of the main button, Defaults to label if not specified.
        // activeLabel: Text("Close Speed Dial"),
        /// Transition Builder between label and activeLabel, defaults to FadeTransition.
        // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
        /// The below button size defaults to 56 itself, its the FAB size + It also affects relative padding and other elements
        buttonSize: 56.0,
        visible: true,

        /// If true user is forced to close dial manually
        /// by tapping main button and overlay is not rendered.
        closeManually: false,

        /// If true overlay will render no matter what.
        renderOverlay: false,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        tooltip: 'Speed Dial',
        heroTag: 'speed-dial-hero-tag',
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        // orientation: SpeedDialOrientation.Up,
        // childMarginBottom: 2,
        // childMarginTop: 2,
        children: [
          SpeedDialChild(
            child: Icon(Icons.calendar_today),
            backgroundColor: Colors.green,
            label: 'Book appointment',
            labelStyle: TextStyle(fontSize: 18.0),
            labelBackgroundColor: Colors.white,
            onTap: () async {
              bool hasConnection =
                  await NetworkUtils.checkNetworkConnectivity();
              if (hasConnection) {
                LocationData currentLocation = await _location.getLocation();
                DatePicker.showDateTimePicker(context,
                    minTime: DateTime.now().add(Duration(hours: 1)),
                    showTitleActions: true,
                    onChanged: (date) {}, onConfirm: (date) async {
                  await UserController.createRequest(
                      widget.serviceProvider,
                      widget.sharedPreferences,
                      widget.user,
                      widget.source,
                      date,
                      currentLocation.latitude,
                      currentLocation.longitude);
                  _showModal();
                }, currentTime: DateTime.now().add(Duration(hours: 1)));
              } else {
                createNetworkError(context, 'book an appointment', () {
                  Navigator.pop(context);
                });
              }
            },
          ),
          SpeedDialChild(
            child: Icon(_isFavorite ? Icons.clear : Icons.favorite),
            backgroundColor: Colors.red,
            label: _isFavorite ? 'Remove from favorites' : 'Add to favorites',
            labelBackgroundColor: Colors.white,
            labelStyle: TextStyle(fontSize: 18.0),
            onTap: () {
              toggleFavorite(context);
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          Column(
            children: <Widget>[topContent, bottomContent],
          )
        ],
      ),
    );
  }
}
