import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/captcha/recaptcha_v2.dart';
import 'package:home_services/controllers/user_controller.dart';
import 'package:home_services/delegates/address_search_delegate.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/notifiers/user_notifier.dart';
import 'package:home_services/pages/login_page.dart';
import 'package:home_services/ui/font_constants.dart';
import 'package:home_services/utils/auth_utils.dart';
import 'package:home_services/utils/form_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:home_services/utils/place_suggestions_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import 'navigation_page.dart';

final _dateProvider = StateProvider<DateTime>((ref) => DateTime.now());
final _latProvider = StateProvider<double>((ref) => 0.0);
final _lngProvider = StateProvider<double>((ref) => 0.0);
final _addressProvider = StateProvider<String>((ref) => "");
final _stepProvider = StateProvider<int>((ref) => 0);
final _completeProvider = StateProvider<bool>((ref) => false);

final _userProvider = ChangeNotifierProvider((ref) => UserNotifier());

class RegistrationPage extends ConsumerWidget with FormUtils, NetworkUtils {
  final SharedPreferences sharedPreferences;
  RegistrationPage(this.sharedPreferences, {Key key}) : super(key: key);
  final recaptchaV2Controller = RecaptchaV2Controller();

  List<GlobalKey<FormState>> _formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  //Properties
  final firstNameController = TextEditingController();
  final phoneNumberController = TextEditingController();
  final idNumberController = TextEditingController();
  final emailAddressController = TextEditingController();
  final passwordController = TextEditingController();
  final repeatPasswordController = TextEditingController();
  final homeAddressController = TextEditingController();
  final cityController = TextEditingController();
  final countryController = TextEditingController();
  final birthDateController = TextEditingController();
  final focusPassword = FocusNode();
  final focusAddress = FocusNode();
  final focusName = FocusNode();
  final DateTime maxDate = DateTime.now();
  @override
  void dispose() {
    firstNameController.dispose();
    phoneNumberController.dispose();
    idNumberController.dispose();
    emailAddressController.dispose();
    passwordController.dispose();
    repeatPasswordController.dispose();
    homeAddressController.dispose();
    cityController.dispose();
    countryController.dispose();
    birthDateController.dispose();
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final selectedDateState = watch(_dateProvider).state;
    final stepProviderState = watch(_stepProvider).state;
    //context.read(_dateProvider).state = maxDate;
    // checkIfLoggedIn(context).then((value) {
    //   print("value $value");
    //   if (value == true) {
    //     createNetworkError(context, "register", () {
    //       SystemNavigator.pop();
    //     });
    //   }
    // });
    List<Widget> basicInfoChildren = createSpacedChildren([
      createFormInput(
          labelName: 'Name',
          controller: firstNameController,
          keyboardType: TextInputType.name,
          validator: (value) {
            if (value == null || value.toString().isEmpty) {
              return "This field is required";
            }
            return null;
          },
          focusNode: focusName),
      createFormInput(
          labelName: 'Phone number',
          hintText: '+57 3215801200',
          suffixIcon: Icons.phone,
          controller: phoneNumberController,
          validator: (value) {
            String pattern =
                r'\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$';
            RegExp regExp = new RegExp(pattern);
            if (value.length == 0) {
              return 'This field is required';
            } else if (!regExp.hasMatch(value)) {
              return 'Please enter a valid mobile number';
            }
            return null;
          },
          keyboardType: TextInputType.phone),
      createFormInput(
          labelName: 'ID Number',
          hintText: '1010254235',
          suffixIcon: Icons.perm_identity_rounded,
          controller: idNumberController,
          validator: (value) {
            String pattern = r'(\d+)';
            RegExp regExp = new RegExp(pattern);
            if (value.length == 0) {
              return 'This field is required';
            } else if (!regExp.hasMatch(value)) {
              return 'Please enter a valid ID number';
            }
            return null;
          },
          keyboardType: TextInputType.number),
      createFormInput(
          labelName: 'Email address',
          hintText: 'i.e team4@mobile.dev',
          suffixIcon: Icons.email_outlined,
          controller: emailAddressController,
          keyboardType: TextInputType.emailAddress,
          validator: (value) {
            final isValid = EmailValidator.validate(value);
            if (!isValid) {
              return "Please enter a valid email address";
            }
            return null;
          }),
      createFormInput(
          labelName: 'Birth Date',
          suffixIcon: Icons.cake_outlined,
          controller: birthDateController,
          keyboardType: TextInputType.datetime,
          readOnly: true,
          validator: (value) {
            if (value.length == 0) {
              return 'This field is required';
            }
            return null;
          },
          onTap: () async {
            // if (birthDateController.text.isNotEmpty) {
            //   var dateArray = birthDateController.text.split("/");
            //   context.read(_dateProvider).state = DateTime(
            //       int.parse(dateArray[2]),
            //       int.parse(dateArray[1]),
            //       int.parse(dateArray[0]));
            // }
            final DateTime picked = await showDatePicker(
              context: context,
              initialDate: selectedDateState,
              firstDate: DateTime(1930),
              lastDate: maxDate,
            );
            if (picked != null && picked != selectedDateState) {
              context.read(_dateProvider).state = picked;
              birthDateController.text =
                  '${picked.day}/${picked.month}/${picked.year}';
            }
            focusPassword.requestFocus();
          }),
      createFormInput(
          labelName: 'Password',
          obscureText: true,
          suffixIcon: Icons.lock_outline_rounded,
          controller: passwordController,
          keyboardType: TextInputType.visiblePassword,
          validator: (value) {
            if (value.length == 0) {
              return 'This field is required';
            }
            if (value.length < 9) {
              return 'The password must be at least 9 characters long';
            }
            return null;
          },
          focusNode: focusPassword),
    ]);
    List<Widget> addressChildren = createSpacedChildren([
      createFormInput(
          labelName: 'Home Address',
          hintText: 'Cra 7 # 12-49',
          focusNode: focusAddress,
          suffixIcon: Icons.house,
          validator: (value) {
            if (value.length == 0) {
              return "This field is required";
            }
            if (context.read(_latProvider).state.toString().isEmpty ||
                context.read(_lngProvider).state.toString().isEmpty) {
              return "This address is invalid. Please enter a more detailed address";
            }
            return null;
          },
          readOnly: true,
          onTap: () async {
            final hasConnection = await NetworkUtils.checkNetworkConnectivity();
            if (!hasConnection) {
              EasyLoading.showError(
                  'No Internet Connection! Unable to search addresses');
              return;
            }
            final sessionToken = Uuid().v4();
            final Suggestion result = await showSearch(
              context: context,
              delegate: AddressSearch(sessionToken),
            );
            // This will change the text displayed in the TextField
            if (result != null) {
              final placeDetails = await PlaceApiProvider(sessionToken)
                  .getPlaceDetailFromId(result.placeId);
              try {
                context.read(_addressProvider).state = result.description;
                context.read(_latProvider).state = placeDetails.lat;
                context.read(_lngProvider).state = placeDetails.lng;
                var strArray = result.description.split(", ");
                homeAddressController.text = strArray[0];
                cityController.text = placeDetails.city ?? strArray[1];
                countryController.text = placeDetails.country ?? strArray[2];
              } catch (err) {
                context.read(_addressProvider).state = result.description;
                homeAddressController.text = result.description;
              }
            }
          },
          controller: homeAddressController),
      createFormInput(
          labelName: 'City',
          hintText: 'Bogotá D.C',
          readOnly: true,
          suffixIcon: Icons.location_city,
          validator: (value) {
            if (value.length == 0) {
              return "This field is required";
            }
            return null;
          },
          controller: cityController),
      createFormInput(
          labelName: 'Country',
          hintText: 'Colombia',
          readOnly: true,
          suffixIcon: Icons.map,
          validator: (value) {
            if (value.length == 0) {
              return "This field is required";
            }
            return null;
          },
          controller: countryController),
    ]);
    Step basicInfoStep = createStep(
        'Basic Info',
        stepProviderState >= 0,
        stepProviderState == 0 ? StepState.editing : StepState.complete,
        [Form(key: _formKeys[0], child: Column(children: basicInfoChildren))]);
    Step addressStep = createStep(
        'Address',
        stepProviderState >= 1,
        stepProviderState == 1 ? StepState.editing : StepState.complete,
        [Form(key: _formKeys[1], child: Column(children: addressChildren))]);
    List<Step> steps = [
      basicInfoStep,
      addressStep,
      Step(
        state: stepProviderState < 2 ? StepState.editing : StepState.complete,
        isActive: stepProviderState == 2,
        title: const Text('Complete'),
        content: Column(
          children: <Widget>[
            Image(
              image: AssetImage('images/registration_success.png'),
            )
          ],
        ),
      ),
    ];
    watch(_completeProvider).state;
    return new Scaffold(
      body: Theme(
        data: ThemeData(
            primaryColor: Colors.lightGreen,
            buttonColor: Colors.green,
            backgroundColor: Colors.white),
        child: SafeArea(
          child: Container(
            child: Stack(
              children: [
                Stepper(
                  type: stepperType,
                  steps: steps,
                  currentStep: stepProviderState,
                  onStepContinue: () => next(context, steps),
                  onStepTapped: (step) {
                    if (step < stepProviderState) {
                      goTo(context, step);
                      recaptchaV2Controller.hide();
                    }
                  },
                  onStepCancel: () => cancel(context),
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue,
                      VoidCallback onStepCancel}) {
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 100.0),
                            Container(
                              width: 360.0,
                              height: 50,
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all(Colors.green),
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            side: BorderSide(
                                                color: Colors.green)))),
                                child: FontConstants.formatDisplayText(
                                    text: 'Next',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 18.0),
                                onPressed: onStepContinue,
                              ),
                            ),
                          ],
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FontConstants.formatDisplayText(
                                  text:
                                      '------------------------  o r  ------------------------',
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                  letterSpacing: -2,
                                  fontSize: 21.0)
                            ]),
                        SizedBox(
                          height: 25,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RichText(
                              text: TextSpan(
                                style: FontConstants.displayStyle(
                                    fontSize: 18.0, color: Colors.grey),
                                children: <TextSpan>[
                                  TextSpan(text: 'Already have an account ?'),
                                  TextSpan(
                                      text: ' Sign In',
                                      style: FontConstants.displayStyle(
                                          fontSize: 18.0,
                                          color: Colors.green,
                                          fontWeight: FontWeight.bold),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => LoginPage(
                                                    sharedPreferences)),
                                          );
                                        }),
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    );
                  },
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: SizedBox(
                        height: 500,
                        width: 500,
                        child: Center(
                          child: RecaptchaV2(
                            apiKey: env[
                                "GOOGLE_CAPTCHA_SITE_KEY"], // for enabling the reCaptcha
                            apiSecret: env[
                                "GOOGLE_CAPTCHA_SECRET_KEY"], // for verifying the responded token
                            controller: recaptchaV2Controller,
                            onVerifiedError: (err) {
                              print(err);
                            },
                            onVerifiedSuccessfully: (success) async {
                              if (success) {
                                recaptchaV2Controller.hide();
                                bool successfulRegister =
                                    await UserController.postUser(User(
                                        name: firstNameController.text,
                                        email: emailAddressController.text,
                                        username: emailAddressController.text,
                                        phone_number:
                                            phoneNumberController.text,
                                        id_number: idNumberController.text,
                                        birth_date:
                                            context.read(_dateProvider).state,
                                        address_lat: double.parse(context
                                            .read(_latProvider)
                                            .state
                                            .toStringAsFixed(6)),
                                        address_lon: double.parse(context
                                            .read(_lngProvider)
                                            .state
                                            .toStringAsFixed(6)),
                                        address_desc: context
                                            .read(_addressProvider)
                                            .state,
                                        password: passwordController.text));
                                if (successfulRegister) {
                                  User loggedUser = await logIn(
                                      context,
                                      _userProvider,
                                      sharedPreferences,
                                      emailAddressController.text,
                                      emailAddressController.text,
                                      passwordController.text);
                                  if (loggedUser != null) {
                                    context
                                        .read(_userProvider.notifier)
                                        .setUser(loggedUser);
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => NavigationPage(
                                              sharedPreferences)),
                                    );
                                  }
                                }
                              }
                            },
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Step createStep(
      String title, bool isActive, StepState state, List<Widget> children) {
    return Step(
      title: Text(title),
      isActive: isActive,
      state: state,
      content: Column(
        children: children,
      ),
    );
  }

  next(BuildContext context, steps) async {
    final hasConnection = await NetworkUtils.checkNetworkConnectivity();
    if (!hasConnection) {
      EasyLoading.showError('No Internet Connection! Unable to register');
      return;
    }
    if (context.read(_stepProvider).state + 1 != steps.length) {
      if (_formKeys[context.read(_stepProvider).state]
          .currentState
          .validate()) {
        goTo(context, context.read(_stepProvider).state + 1);
      }
    } else {
      context.read(_completeProvider).state = true;
      recaptchaV2Controller.show();
    }
  }

  cancel(BuildContext context) {
    if (context.read(_stepProvider).state > 0) {
      goTo(context, context.read(_stepProvider).state - 1);
    }
  }

  goTo(BuildContext context, int step) {
    print("Go to $step -- ${context.read(_stepProvider).state}");
    context.read(_stepProvider).state = step;
    if (step == 0) {
      focusName.requestFocus();
    } else if (step == 1) {
      focusAddress.requestFocus();
      focusAddress.unfocus();
    }
  }

  StepperType stepperType = StepperType.horizontal;
}
