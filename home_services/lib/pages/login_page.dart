import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/notifiers/user_notifier.dart';
import 'package:home_services/pages/navigation_page.dart';
import 'package:home_services/pages/registration_page.dart';
import 'package:home_services/ui/font_constants.dart';
import 'package:home_services/utils/auth_utils.dart';
import 'package:home_services/utils/form_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

final _userProvider = ChangeNotifierProvider((ref) => UserNotifier());

final _stepProvider = StateProvider<int>((ref) => 0);

class LoginPage extends ConsumerWidget with FormUtils, NetworkUtils {
  final _formKey = GlobalKey<FormState>();
  final SharedPreferences sharedPreferences;
  LoginPage(this.sharedPreferences, {Key key}) : super(key: key);

  final emailAddressController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    emailAddressController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final stepProviderState = watch(_stepProvider).state;
    List<Widget> loginChildren = createSpacedChildren([
      createFormInput(
          labelName: 'Email address',
          hintText: 'i.e team4@mobile.dev',
          suffixIcon: Icons.email,
          controller: emailAddressController,
          validator: (value) {
            final isValid = EmailValidator.validate(value);
            if (!isValid) {
              return "Please enter a valid email address";
            }
            return null;
          }),
      createFormInput(
          labelName: 'Password',
          obscureText: true,
          suffixIcon: Icons.lock,
          controller: passwordController,
          validator: (value) {
            if (value.length == 0) {
              return 'This field is required';
            }
            if (value.length < 9) {
              return 'The password must be at least 9 characters long';
            }
            return null;
          })
    ]);
    Step loginStep = createStep(
        'Login',
        stepProviderState >= 0,
        stepProviderState == 0 ? StepState.editing : StepState.complete,
        [Form(key: _formKey, child: Column(children: loginChildren))]);
    List<Step> steps = [loginStep];
    return new Scaffold(
      body: Theme(
        data: ThemeData(
            primaryColor: Colors.lightGreen,
            buttonColor: Colors.green,
            backgroundColor: Colors.white),
        child: SafeArea(
          child: Container(
            child: Stepper(
              type: stepperType,
              steps: steps,
              currentStep: stepProviderState,
              onStepContinue: () => next(context),
              onStepTapped: (step) => goTo(context, step),
              onStepCancel: () => cancel(context),
              controlsBuilder: (BuildContext context,
                  {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                return Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: 100.0),
                        Container(
                          width: 360.0,
                          height: 50,
                          child: ElevatedButton(
                            style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.green),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        side:
                                            BorderSide(color: Colors.green)))),
                            child: FontConstants.formatDisplayText(
                                text: 'Next',
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 18.0),
                            onPressed: onStepContinue,
                          ),
                        ),
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      FontConstants.formatDisplayText(
                          text:
                              '------------------------  o r  ------------------------',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey,
                          letterSpacing: -2,
                          fontSize: 21.0),
                    ]),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RichText(
                          text: TextSpan(
                            style: FontConstants.displayStyle(
                                fontSize: 18.0, color: Colors.grey),
                            children: <TextSpan>[
                              TextSpan(text: 'Don\'t have an account ?'),
                              TextSpan(
                                  text: ' Sign Up',
                                  style: FontConstants.displayStyle(
                                      fontSize: 18.0,
                                      color: Colors.green,
                                      fontWeight: FontWeight.bold),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RegistrationPage(
                                                    sharedPreferences)),
                                      );
                                    }),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Step createStep(
      String title, bool isActive, StepState state, List<Widget> children) {
    return Step(
      title: Text(title),
      isActive: isActive,
      state: state,
      content: Column(
        children: children,
      ),
    );
  }

  next(BuildContext context) async {
    final hasConnection = await NetworkUtils.checkNetworkConnectivity();
    if (!hasConnection) {
      EasyLoading.showError('No Internet Connection! Unable to log in');
      return;
    }
    if (_formKey.currentState.validate()) {
      User loggedUser = await logIn(
          context,
          _userProvider,
          sharedPreferences,
          emailAddressController.text,
          emailAddressController.text,
          passwordController.text);
      if (loggedUser != null) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NavigationPage(sharedPreferences)),
        );
      }
    }
  }

  cancel(BuildContext context) {
    if (context.read(_stepProvider).state > 0) {
      goTo(context, context.read(_stepProvider).state - 1);
    }
  }

  goTo(BuildContext context, int step) {
    context.read(_stepProvider).state = step;
  }

  StepperType stepperType = StepperType.horizontal;
}
