import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/notifiers/user_notifier.dart';
import 'package:home_services/pages/registration_page.dart';
import 'package:home_services/pages/search_service_providers_page.dart';
import 'package:home_services/utils/auth_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'calendar_page.dart';
import 'login_page.dart';
import 'maps_page.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

final _userProvider = ChangeNotifierProvider((ref) => UserNotifier());

bool enforceLogin(
    BuildContext context, bool loggedIn, SharedPreferences sharedPreferences) {
  if (!loggedIn) {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => RegistrationPage(sharedPreferences)),
      );
    });
    return false;
  }
  return true;
}

class NavigationPage extends ConsumerWidget {
  final SharedPreferences sharedPreferences;
  NavigationPage(this.sharedPreferences, {Key key}) : super(key: key);

  PageController _myPage = PageController(initialPage: 5);
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final userState = watch(_userProvider).user;
    final Future _futureRecoverLogin =
        recoverLogin(context, sharedPreferences, _userProvider);
    return FutureBuilder(future: _futureRecoverLogin.then((data) {
      return enforceLogin(context, !userState.isNewUser(), sharedPreferences);
    }), builder: (BuildContext context, AsyncSnapshot snapshot) {
      if (!snapshot.hasData) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            bottomNavigationBar: BottomAppBar(
              shape: CircularNotchedRectangle(),
              child: Container(
                height: 75,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      iconSize: 30.0,
                      padding: EdgeInsets.only(left: 28.0),
                      icon: Icon(Icons.person_search),
                      onPressed: () => _myPage.jumpToPage(0),
                    ),
                    IconButton(
                      iconSize: 30.0,
                      padding: EdgeInsets.only(right: 28.0),
                      icon: Icon(Icons.table_rows),
                      onPressed: () => _myPage.jumpToPage(0),
                    ),
                    IconButton(
                      iconSize: 30.0,
                      padding: EdgeInsets.only(left: 28.0),
                      icon: Icon(Icons.calendar_today),
                      onPressed: () => _myPage.jumpToPage(2),
                    ),
                    IconButton(
                      iconSize: 30.0,
                      padding: EdgeInsets.only(right: 28.0),
                      icon: Icon(Icons.account_circle),
                      onPressed: () {
                        EasyLoading.showInfo("Closing session in 3 seconds...");
                        Timer(Duration(seconds: 3), () {
                          sharedPreferences.clear();
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    LoginPage(sharedPreferences)),
                          );
                        });
                      },
                    )
                  ],
                ),
              ),
            ),
            body: PageView(
              controller: _myPage,
              onPageChanged: (int) {
                print('Page Changes to index $int');
              },
              children: <Widget>[
                Center(
                  child: Container(
                    child: SearchServiceProvidersPage(
                        sharedPreferences: sharedPreferences, user: userState),
                  ),
                ),
                Center(
                  child: Container(
                      child: SearchServiceProvidersPage(
                          sharedPreferences: sharedPreferences,
                          user: userState)),
                ),
                Center(
                  child: Container(
                      child: CalendarPage(sharedPreferences, userState)),
                ),
                Center(
                  child: Container(
                    child: LoginPage(sharedPreferences),
                  ),
                ),
                Center(
                  child:
                      Container(child: MapsPage(sharedPreferences, userState)),
                ),
              ],
              physics:
                  NeverScrollableScrollPhysics(), // Comment this if you need to use Swipe.
            ),
            floatingActionButton: Container(
              height: 65.0,
              width: 65.0,
              child: FittedBox(
                child: FloatingActionButton(
                  backgroundColor: Colors.green,
                  onPressed: () => _myPage.jumpToPage(4),
                  child: Icon(
                    Icons.online_prediction,
                    color: Colors.white,
                  ),
                  // elevation: 5.0,
                ),
              ),
            ),
          ),
        );
      }
    });
  }
}
