import 'dart:developer';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_services/controllers/service_provider_controller.dart';
import 'package:home_services/models/service_provider.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/pages/service_provider_detail_page.dart';
import 'package:home_services/ui/font_constants.dart';
import 'package:home_services/utils/crypto_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchServiceProvidersPage extends StatefulWidget {
  SearchServiceProvidersPage(
      {Key key, this.title, this.sharedPreferences, this.user})
      : super(key: key);
  final String title;
  final SharedPreferences sharedPreferences;
  final User user;

  @override
  _SearchServiceProvidersPageState createState() =>
      new _SearchServiceProvidersPageState();
}

class _SearchServiceProvidersPageState extends State<SearchServiceProvidersPage>
    with AutomaticKeepAliveClientMixin<SearchServiceProvidersPage> {
  @override
  bool get wantKeepAlive => true;
  String _sortValue = 'Default';
  String _currentServiceCategory = 'All';
  String _sortOrder = 'Ascending';
  String _currentQuery = '';
  bool _loadedRecommended = false;
  TextEditingController editingController = TextEditingController();

  List<ServiceProvider> duplicateItems = [];
  List<ServiceProvider> items = [];

  @override
  void initState() {
    loadProviders();
    super.initState();
  }

  void loadProviders() async {
    String msg = 'Loading near service providers...';
    EasyLoading.show(status: msg);
    var localProviders =
        await ServiceProviderController.fetchLocalClosestProviders(
            widget.sharedPreferences);
    if (localProviders != null) {
      setState(() {
        items.clear();
        duplicateItems.clear();
        items.addAll(localProviders);
        duplicateItems.addAll(localProviders);
        _loadedRecommended = false;
      });
      EasyLoading.dismiss();
    }
    final hasConnection = await NetworkUtils.checkNetworkConnectivity();

    if (!hasConnection && localProviders != null)
      EasyLoading.showSuccess('No Internet Connection! Local data loaded');
    else if (!hasConnection && localProviders == null) {
      EasyLoading.showError('Near service providers could not be loaded');
    }

    var remoteProviders;
    try {
      remoteProviders = await ServiceProviderController.fetchClosestProviders(
          widget.sharedPreferences, widget.user);
      print("Remote providers $remoteProviders");
    } on SocketException catch (err) {
      if (hasConnection)
        EasyLoading.showError("Error loading the service providers");
    } catch (err) {}
    log("local -> ${localProviders.toString()}");
    log("remote -> ${remoteProviders.toString()}");
    if (remoteProviders != null &&
        !checkIfEqual(localProviders, remoteProviders)) {
      EasyLoading.showInfo('New near service providers found!');
      setState(() {
        items.clear();
        duplicateItems.clear();
        items.addAll(remoteProviders);
        duplicateItems.addAll(remoteProviders);
        _loadedRecommended = false;
      });
      EasyLoading.dismiss();
    } else {}

    EasyLoading.dismiss();
  }

  void loadRecommendedProviders() async {
    String msg = 'Loading recommended service providers...';
    EasyLoading.show(status: msg);
    var localProviders =
        await ServiceProviderController.fetchLocalRecommendedProviders(
            widget.sharedPreferences);
    if (localProviders != null) {
      setState(() {
        items.clear();
        duplicateItems.clear();
        items.addAll(localProviders);
        duplicateItems.addAll(localProviders);
        _loadedRecommended = true;
      });
      EasyLoading.dismiss();
    }
    final hasConnection = await NetworkUtils.checkNetworkConnectivity();

    if (!hasConnection && localProviders != null)
      EasyLoading.showSuccess('No Internet Connection! Local data loaded');
    else if (!hasConnection && localProviders == null) {
      EasyLoading.showError(
          'Recommended service providers could not be loaded');
    }

    var remoteProviders;
    try {
      remoteProviders =
          await ServiceProviderController.fetchRecommendedProviders(
              widget.sharedPreferences, widget.user);
      print("Remote providers $remoteProviders");
    } catch (err) {
      if (hasConnection)
        EasyLoading.showError("Error loading the recommended providers");
    }

    log("local -> ${localProviders.toString()}");
    log("remote -> ${remoteProviders.toString()}");
    if (remoteProviders != null &&
        !checkIfEqual(localProviders, remoteProviders)) {
      EasyLoading.showInfo('New recommended service providers found!');
      setState(() {
        items.clear();
        duplicateItems.clear();
        items.addAll(remoteProviders);
        duplicateItems.addAll(remoteProviders);
        _loadedRecommended = true;
      });
      EasyLoading.dismiss();
    } else {}

    EasyLoading.dismiss();
  }

  void filterSearchResults() {
    List<ServiceProvider> dummySearchList = [];
    List<ServiceProvider> dummyListData = [];
    bool changeState = false;
    dummySearchList.addAll(duplicateItems);
    if (_currentServiceCategory.isNotEmpty &&
        _currentQuery.isNotEmpty &&
        _currentServiceCategory != 'All') {
      dummySearchList.forEach((serviceProvider) {
        if (serviceProvider.contains(_currentQuery) &&
            serviceProvider.contains(_currentServiceCategory)) {
          dummyListData.add(serviceProvider);
        }
      });
      changeState = true;
    } else if (_currentQuery.isNotEmpty) {
      dummySearchList.forEach((item) {
        if (item.contains(_currentQuery)) {
          dummyListData.add(item);
        }
      });
      changeState = true;
    } else if (_currentServiceCategory.isNotEmpty &&
        _currentServiceCategory != 'All') {
      dummySearchList.forEach((item) {
        if (item.contains(_currentServiceCategory)) {
          dummyListData.add(item);
        }
      });
      changeState = true;
    }
    setState(() {
      items.clear();
      if (_sortValue != 'Default') {
        String attributeToSort;
        switch (_sortValue) {
          case 'Name':
            attributeToSort = 'first_name';
            break;
          case 'Rating':
          default:
            attributeToSort = 'rating';
            break;
        }
        if (changeState) {
          dummyListData.sort((ServiceProvider a, ServiceProvider b) =>
              a.compareTo(b, attributeToSort, _sortOrder));
        } else {
          duplicateItems.sort((ServiceProvider a, ServiceProvider b) =>
              a.compareTo(b, attributeToSort, _sortOrder));
        }
      }
      items.addAll(changeState ? dummyListData : duplicateItems);
    });
  }

  var _services = [
    'All',
    'Carpentry',
    'Electricity',
    'Housekeeping',
    'Locksmith',
    'Plumbing'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.filter_list, color: Colors.black),
            onPressed: () {
              showSortByDialog(context);
            },
          ),
        ],
        title: FontConstants.formatAppBar(
          text: 'SEARCH',
          color: Colors.black,
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  height: 55.0,
                  child: FormField<String>(
                    builder: (FormFieldState<String> state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                            errorStyle: TextStyle(color: Colors.redAccent),
                            hintText: 'Please select service category',
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0))),
                        isEmpty: _currentServiceCategory == '',
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            icon: Icon(
                              Icons.arrow_downward,
                              size: 18.09,
                            ),
                            hint: Container(
                              width: 150,
                              child: Text(
                                'Service category',
                              ),
                            ),
                            value: _currentServiceCategory,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                _currentServiceCategory = newValue;
                                filterSearchResults();
                                state.didChange(newValue);
                              });
                            },
                            items: _services.map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Container(
                                    width: 150,
                                    child: Text(
                                      value,
                                    )),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                )),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextField(
                onChanged: (value) {
                  setState(() {
                    _currentQuery = value;
                  });
                  filterSearchResults();
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: 'Search',
                    hintText: "Enter your keywords",
                    suffixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                width: 360.0,
                height: 50,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.green),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: Colors.green)))),
                  child: FontConstants.formatDisplayText(
                      text: _loadedRecommended
                          ? 'Load near providers'
                          : 'Load providers recommended for you',
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 18.0),
                  onPressed: () {
                    _loadedRecommended
                        ? loadProviders()
                        : loadRecommendedProviders();
                  },
                ),
              ),
            ),
            SizedBox(height: 10),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: AspectRatio(
                      aspectRatio: 1,
                      child: CachedNetworkImage(
                          imageUrl: items[index].profile_picture,
                          fit: BoxFit.scaleDown,
                          height: 32),
                    ),
                    title: Text('${items[index].name}'),
                    subtitle: Text(
                        '${(items[index].category != null && items[index].category[0] != null) ? items[index].category[0] : "None"}\n${items[index].rating != null ? "Rating: ${items[index].rating}" : "No rating available"}'),
                    trailing: Icon(Icons.more_vert),
                    isThreeLine: true,
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ServiceProviderDetailPage(
                                  sharedPreferences: widget.sharedPreferences,
                                  serviceProvider: items[index],
                                  source: "SH",
                                  user: widget.user)));
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> showSortByDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext build) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Center(
                  child: Text(
                "Sort by",
                style: TextStyle(color: Colors.black),
              )),
              content: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 12, right: 10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: Icon(
                              Icons.sort,
                              color: Color(0xff808080),
                            ),
                          ),
                          Expanded(
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                hint: Text("Sort by"),
                                items: <String>["Default", "Name", "Rating"]
                                    .map((String value) {
                                  return DropdownMenuItem(
                                    value: value,
                                    child: Text(value,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 16)),
                                  );
                                }).toList(),
                                value: _sortValue,
                                onChanged: (newValue) {
                                  setState(() {
                                    _sortValue = newValue;
                                  });
                                  filterSearchResults();
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 8, right: 10),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right: 16.0),
                            child: Icon(
                              Icons.sort_by_alpha,
                              color: Color(0xff808080),
                            ),
                          ),
                          Expanded(
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                items: <String>[
                                  "Ascending",
                                  "Descending",
                                ].map((String value) {
                                  return DropdownMenuItem(
                                    value: value,
                                    child: Text(value,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 16)),
                                  );
                                }).toList(),
                                value: _sortOrder,
                                onChanged: (newValue) {
                                  setState(() {
                                    _sortOrder = newValue;
                                  });
                                  filterSearchResults();
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          });
        });
  }
}
