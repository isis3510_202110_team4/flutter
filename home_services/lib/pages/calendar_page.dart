import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/controllers/user_controller.dart';
import 'package:home_services/models/service.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/notifiers/user_notifier.dart';
import 'package:home_services/utils/local_db_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:table_calendar/table_calendar.dart';

final _userProvider = ChangeNotifierProvider((ref) => UserNotifier());

final kNow = DateTime.now();
final kFirstDay = DateTime(kNow.year, kNow.month - 3, kNow.day);
final kLastDay = DateTime(kNow.year, kNow.month + 3, kNow.day);

final _focusedDayProvider = StateProvider<DateTime>((ref) => DateTime.now());
final _selectedDayProvider = StateProvider<DateTime>((ref) => DateTime.now());

class CalendarPage extends ConsumerWidget {
  final SharedPreferences sharedPreferences;
  final User user;
  CalendarPage(this.sharedPreferences, this.user, {Key key}) : super(key: key);

  int getHashCode(DateTime key) {
    return key.day * 1000000 + key.month * 10000 + key.year;
  }

  LinkedHashMap<DateTime, List<Service>> _groupedEvents;
  _groupEvents(List<Service> events) {
    _groupedEvents = LinkedHashMap(equals: isSameDay, hashCode: getHashCode);
    events.forEach((event) {
      DateTime date = DateTime.utc(event.date_scheduled.year,
          event.date_scheduled.month, event.date_scheduled.day, 12);
      if (_groupedEvents[date] == null) _groupedEvents[date] = [];
      _groupedEvents[date].add(event);
    });
  }

  List<dynamic> _getEventsForDay(DateTime date) {
    return _groupedEvents[date] ?? [];
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final userState = watch(_userProvider);
    final selectedDayState = watch(_selectedDayProvider).state;
    final focusedDayState = watch(_focusedDayProvider).state;
    return Scaffold(
      body: FutureBuilder(
        future:
            UserController.getServices(sharedPreferences, user).then((events) {
          if (events.length > 0) {
            return events;
          }
          if (events.length == 0) {
            return NetworkUtils.checkNetworkConnectivity()
                .then((hasConnection) {
              if (!hasConnection) {
                // EasyLoading.showInfo(
                //     'No Internet Connection! Attempting to load previously retrieved services');
                return retrieveDataFromDb(sharedPreferences, "getServices")
                    .then((data) {
                  if (data == null) {
                    // EasyLoading.showError(
                    //     "Couldn't load previously retrieved services");
                    return new List<Service>();
                  }
                  // EasyLoading.showSuccess("Retrieved local data successfully");
                  Iterable list = json.decode(data);
                  return List<Service>.from(list.map((model) {
                    return Service.fromJson(model);
                  }));
                });
              }
              return events;
            });
          }
        }),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            final events = snapshot.data;
            print("events retrieved $events");

            _groupEvents(events);
            DateTime selectedDate = selectedDayState;
            final _selectedEvents = _groupedEvents[selectedDate] ?? [];
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  Card(
                    clipBehavior: Clip.antiAlias,
                    margin: const EdgeInsets.all(8.0),
                    child: TableCalendar(
                      focusedDay: focusedDayState,
                      selectedDayPredicate: (day) =>
                          isSameDay(day, selectedDayState),
                      firstDay: kFirstDay,
                      lastDay: kLastDay,
                      eventLoader: _getEventsForDay,
                      onDaySelected: (selectedDay, focusedDay) {
                        context.read(_selectedDayProvider).state = selectedDay;
                        context.read(_focusedDayProvider).state = focusedDay;
                      },
                      weekendDays: [6],
                      headerStyle: HeaderStyle(
                        decoration: BoxDecoration(
                          color: Colors.blue,
                        ),
                        headerMargin: const EdgeInsets.only(bottom: 8.0),
                        titleTextStyle: TextStyle(
                          color: Colors.white,
                        ),
                        formatButtonDecoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        formatButtonTextStyle: TextStyle(color: Colors.white),
                        leftChevronIcon: Icon(
                          Icons.chevron_left,
                          color: Colors.white,
                        ),
                        rightChevronIcon: Icon(
                          Icons.chevron_right,
                          color: Colors.white,
                        ),
                      ),
                      calendarStyle: CalendarStyle(),
                      calendarBuilders: CalendarBuilders(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0, top: 8.0),
                    child: Text(
                      DateFormat('EEEE, dd MMMM, yyyy').format(selectedDate),
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: _selectedEvents.length,
                    itemBuilder: (BuildContext context, int index) {
                      Service event = _selectedEvents[index];
                      return ListTile(
                        title: Text("${event.category} - ${event.workerName}"),
                        subtitle: Text("${event.desc}"),
                        onTap: () {},
                      );
                    },
                  ),
                ],
              ),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
