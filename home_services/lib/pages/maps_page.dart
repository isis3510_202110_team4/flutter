import 'dart:collection';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:home_services/controllers/service_provider_controller.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/notifiers/service_provider_notifier.dart';
import 'package:home_services/pages/service_provider_detail_page.dart';
import 'package:home_services/ui/font_constants.dart';
import 'package:home_services/utils/crypto_utils.dart';
import 'package:home_services/utils/local_db_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/service_provider.dart';

final _serviceProviderProvider =
    ChangeNotifierProvider((ref) => ServiceProviderNotifier());

final _markersProvider = StateProvider<Set<Marker>>((ref) => new HashSet());
final _circlesProvider = StateProvider<Set<Circle>>((ref) => new HashSet());
final _serviceProvider = StateProvider<List<dynamic>>((ref) => []);
final _mapLoaded = StateProvider<bool>((ref) => false);

class MapsPage extends ConsumerWidget with NetworkUtils {
  final SharedPreferences sharedPreferences;
  final User user;
  MapsPage(this.sharedPreferences, this.user, {Key key}) : super(key: key);
  GoogleMapController mapController;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  PageController _pageController;

  int prevPage;

  final LatLng _initialLocation = const LatLng(4.6482837, -74.2478918);
  Location _location = Location();

  // Set Markers to the map
  void _placeServiceProvider(
      BuildContext context, ServiceProvider serviceProvider) {
    final LatLng point =
        LatLng(serviceProvider.address_lat, serviceProvider.address_lon);
    final double radius = serviceProvider.service_radius * 1000.0;
    final String name = '${serviceProvider.name}';
    final String category = serviceProvider.category.length > 0
        ? serviceProvider.category[0]
        : "None";
    final String serviceProviderId = '${point.latitude}-${point.longitude}';

    final newCirclesState = new HashSet<Circle>()
      ..addAll(context.read(_circlesProvider).state);
    newCirclesState.add(Circle(
      circleId: CircleId(serviceProviderId),
      center: point,
      radius: radius,
      fillColor: Colors.grey.withOpacity(0.3),
      strokeWidth: 3,
      strokeColor: Colors.grey,
    ));
    context.read(_circlesProvider).state = newCirclesState;

    final newMarkersState = new HashSet<Marker>()
      ..addAll(context.read(_markersProvider).state);
    newMarkersState.add(
      Marker(
          visible: true,
          markerId: MarkerId(serviceProviderId),
          position: point,
          infoWindow: InfoWindow(
              title: name,
              snippet: 'Service category: $category',
              onTap: () {
                context
                    .read(_serviceProviderProvider.notifier)
                    .setServiceProvider(serviceProvider);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ServiceProviderDetailPage(
                            serviceProvider: serviceProvider,
                            sharedPreferences: sharedPreferences,
                            user: user,
                            source: "MP")));
              })), // Marker
    );
    context.read(_markersProvider).state = newMarkersState;
  }

  //Check Location Permissions, and get my location
  void _checkLocationPermission() async {
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
  }

  void _onScroll(BuildContext context) {
    if (_pageController.page.toInt() != prevPage) {
      prevPage = _pageController.page.toInt();
      moveCamera(context);
    }
  }

  moveCamera(BuildContext context) {
    if (context.read(_serviceProvider).state.length > 0 &&
        context.read(_serviceProvider).state[_pageController.page.toInt()] !=
            null)
      mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(
              context
                  .read(_serviceProvider)
                  .state[_pageController.page.toInt()]
                  .address_lat,
              context
                  .read(_serviceProvider)
                  .state[_pageController.page.toInt()]
                  .address_lon),
          zoom: 14.0,
          bearing: 45.0,
          tilt: 45.0)));
  }

  void _onMapCreated(BuildContext context, GoogleMapController controller,
      User userState) async {
    context.read(_mapLoaded).state = false;
    mapController = controller;
    _pageController = PageController(initialPage: 1, viewportFraction: 0.8)
      ..addListener(() => _onScroll(context));
    _currentLocation();
    String msg = 'Loading near service providers...';
    EasyLoading.show(status: msg);
    final hasConnection = await NetworkUtils.checkNetworkConnectivity();
    Future.delayed(Duration(seconds: 3), () {
      try {
        EasyLoading.dismiss();
      } catch (err) {}
    });
    if (!hasConnection)
      EasyLoading.showInfo(
          'No Internet Connection! Attempting to load previously retrieved service providers');

    var localProviders =
        await retrieveDataFromDb(sharedPreferences, "fetchClosestProviders");
    var listOfProviders = [];
    if (localProviders != null) {
      print("localProviders ${localProviders.toString()}");
      localProviders.forEach((provider) {
        _placeServiceProvider(context, ServiceProvider.fromJson(provider));
        listOfProviders.add(ServiceProvider.fromJson(provider));
      });
      context.read(_serviceProvider).state = listOfProviders;
      EasyLoading.dismiss();
      context.read(_mapLoaded).state = true;
    }

    if (!hasConnection) {
      if (localProviders != null)
        EasyLoading.showSuccess('No Internet Connection! Local data loaded');
      else {
        EasyLoading.showError('Near service providers could not be loaded');
        return;
      }
    }

    var remoteProviders;
    try {
      remoteProviders = await ServiceProviderController.fetchClosestProviders(
          sharedPreferences, userState);
      print("Remote providers $remoteProviders");
    } on SocketException catch (err) {
      if (hasConnection)
        EasyLoading.showError("Error loading the service providers");
    } catch (err) {}
    if (!checkIfEqual(localProviders, remoteProviders) &&
        remoteProviders != null) {
      EasyLoading.showInfo('New near service providers found!');
      context.read(_circlesProvider).state = new HashSet();
      context.read(_markersProvider).state = new HashSet();
      remoteProviders.forEach((provider) {
        _placeServiceProvider(context, provider);
      });
      context.read(_serviceProvider).state = remoteProviders;
      EasyLoading.dismiss();
    }
    // else {
    //   //TODO: Comment this line
    //   EasyLoading.showSuccess('Data is equal!');
    // }
  }

  void _currentLocation() async {
    LocationData currentLocation = await _location.getLocation();
    mapController.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 17.0,
      ),
    ));
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final userState = user;
    //_checkLocationPermission();
    final Future _futureLoad =
        Future.delayed((Duration(seconds: 2)), () => true);
    final circlesState = watch(_circlesProvider).state;
    final markersState = watch(_markersProvider).state;
    final serviceProviderState = watch(_serviceProvider).state;
    final mapLoadedState = watch(_mapLoaded).state;
    return MaterialApp(
      home: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          floatingActionButton: FloatingActionButton(
            heroTag: "CurrentLocation",
            onPressed: _currentLocation,
            child: Icon(Icons.my_location, color: Colors.black),
            backgroundColor: Colors.white,
          ),
          appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            title: FontConstants.formatAppBar(
              text: 'AROUND ME',
              color: Colors.white,
            ),
            backgroundColor: Colors.transparent,
          ),
          body: Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(children: [
              Expanded(
                child: Stack(
                  children: [
                    GoogleMap(
                      onMapCreated: (controller) =>
                          _onMapCreated(context, controller, userState),
                      myLocationEnabled: true,
                      mapToolbarEnabled: false,
                      zoomControlsEnabled: false,
                      myLocationButtonEnabled: false,
                      circles: circlesState,
                      markers: markersState,
                      padding: EdgeInsets.only(
                        top: 155.0,
                      ),
                      initialCameraPosition: CameraPosition(
                        target: _initialLocation,
                        zoom: 13.0,
                      ),
                    ),
                    mapLoadedState
                        ? FutureBuilder(
                            future: Future.delayed(
                                Duration(seconds: 2), () => true),
                            builder: (context, snapshot) {
                              if (snapshot.hasData) {
                                return Positioned(
                                  bottom: 20.0,
                                  child: Container(
                                    height: 200.0,
                                    width: MediaQuery.of(context).size.width,
                                    child: PageView.builder(
                                      controller: _pageController,
                                      itemCount:
                                          watch(_serviceProvider).state.length,
                                      itemBuilder:
                                          (BuildContext ctx, int index) {
                                        return _serviceProviderList(
                                            ctx, index, serviceProviderState);
                                      },
                                    ),
                                  ),
                                );
                              }
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            })
                        : Container(),
                    Container(
                      height: 175,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: <Color>[
                            Color(0xff388DCA),
                            Colors.white.withOpacity(0.0)
                          ])),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  _serviceProviderList(context, index, serviceProviderState) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ServiceProviderDetailPage(
                        serviceProvider: serviceProviderState[index],
                        sharedPreferences: sharedPreferences,
                        user: user,
                        source: "MP")));
          },
          child: Stack(children: [
            Center(
                child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    height: 125.0,
                    width: 275.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(0.0, 4.0),
                            blurRadius: 10.0,
                          ),
                        ]),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white),
                        child: Row(children: [
                          Container(
                              height: 90.0,
                              width: 90.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0),
                                      topLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                      image: CachedNetworkImageProvider(
                                          serviceProviderState[index]
                                              .profile_picture),
                                      fit: BoxFit.cover))),
                          SizedBox(width: 5.0),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  serviceProviderState[index].name,
                                  style: TextStyle(
                                      fontSize: 12.5,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  serviceProviderState[index].email,
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    serviceProviderState[index].phone_number,
                                    style: TextStyle(
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                )
                              ])
                        ]))))
          ])),
    );
  }
}
