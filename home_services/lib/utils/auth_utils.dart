import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/controllers/user_controller.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/pages/navigation_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'local_db_utils.dart';

Future<bool> checkIfLoggedIn(
    BuildContext context, SharedPreferences sharedPreferences) async {
  User user = await UserController.getCurrentUser(sharedPreferences);
  bool newUser = user.isNewUser();
  if (newUser) {
    EasyLoading.showInfo("Logged in as ${user.name} with id ${user.id}");
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
          builder: (context) => NavigationPage(sharedPreferences)),
    );
  }

  return newUser;
}

recoverLogin(BuildContext context, SharedPreferences sharedPreferences,
    userProvider) async {
  User loggedUser = new User();
  try {
    User userState = context.read(userProvider).user;
    if (userState != null && userState.id != null && userState.id > 0) {
      return userState;
    }

    var userRetrieved = await UserController.getCurrentUser(sharedPreferences);
    if (userRetrieved != null) {
      loggedUser = userRetrieved;
    }
    if (!loggedUser.isNewUser()) {
      context.read(userProvider.notifier).setUser(loggedUser);
      print("Set state ${loggedUser.toString()}");
    }
  } catch (err) {
    print(err);
  }
  return loggedUser;
}

Future<User> logIn(
    BuildContext context,
    _userProvider,
    SharedPreferences sharedPreferences,
    String email,
    String username,
    String password) async {
  User loggedUser = await UserController.login(sharedPreferences,
      User(email: email, username: username, password: password));
  if (loggedUser != null) {
    context.read(_userProvider.notifier).setUser(loggedUser);
  }
  return loggedUser;
}

Future<User> getUser(SharedPreferences sharedPreferences) async {
  var data = await retrieveDataFromDb(sharedPreferences, "currentUser");
  print("GET USER DB, $data");
  if (data != null) {
    try {
      return data as User;
    } catch (err) {
      return User.fromJson(data);
    }
  }
  User usr = await UserController.getCurrentUser(sharedPreferences);
  return usr;
}
