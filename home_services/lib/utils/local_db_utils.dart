import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';
import 'package:shared_preferences/shared_preferences.dart';

String dbPath = 'home_services.db';

Future<int> storeToLocalDb(dynamic map, String petitionId) async {
  DatabaseFactory dbFactory = databaseFactoryIo;
  Directory appDocDirectory = await getApplicationDocumentsDirectory();
  Database db = await dbFactory.openDatabase(appDocDirectory.path + "/$dbPath");

  var store = intMapStoreFactory.store();
  var key = await store.add(db, {"data": map});
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  sharedPreferences.setInt(petitionId, key);
  return key;
}

retrieveDataFromDb(
    SharedPreferences sharedPreferences, String petitionId) async {
  var key = sharedPreferences.getInt(petitionId);
  print("Retrieving $key from query $petitionId");
  if (key != null && !key.isNaN && key > -1) {
    DatabaseFactory dbFactory = databaseFactoryIo;
    Directory appDocDirectory = await getApplicationDocumentsDirectory();
    Database db =
        await dbFactory.openDatabase(appDocDirectory.path + "/$dbPath");

    var store = intMapStoreFactory.store();
    var data = await store.record(key).getSnapshot(db);
    return data["data"];
  }
  return null;
}

Future<bool> storeAPIResponse(
    map, petitionId, SharedPreferences sharedPreferences) async {
  String currentData = json.encode(map);
  List<int> bytesCurrent = utf8.encode(currentData);
  String hashCurrent = sha256.convert(bytesCurrent).toString();

  var retrievedData = await retrieveDataFromDb(sharedPreferences, petitionId);
  if (retrievedData != null) {
    String stringifiedData = json.encode(retrievedData);
    List<int> bytesRetrieved = utf8.encode(stringifiedData);
    String hashRetrieved = sha256.convert(bytesRetrieved).toString();
    if (hashCurrent != hashRetrieved) {
      storeToLocalDb(map, petitionId);
    }
    return hashCurrent == hashRetrieved;
  } else {
    storeToLocalDb(map, petitionId);
    return false;
  }
}
