import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_services/utils/local_db_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class NetworkUtils {
  static Future<bool> checkNetworkConnectivity() async {
    try {
      final result = await InternetAddress.lookup(env['API_ENDPOINT']);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

  Future<void> createNetworkError(
      context, String action, Function onPressed) async {
    final hasConnection = await checkNetworkConnectivity();
    if (!hasConnection)
      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No connection!'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('You need a Internet connection to $action'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('Ok'),
                onPressed: onPressed ??
                    () {
                      SystemNavigator.pop();
                    },
              ),
            ],
          );
        },
      );
  }

  attemptToRecoverLocalData(
      context,
      action,
      onPressed,
      bool throwError,
      String petitionId,
      String errMessage,
      String successMessage,
      SharedPreferences sharedPreferences) async {
    final hasConnection = await checkNetworkConnectivity();
    if (!hasConnection) {
      EasyLoading.show(status: 'Attempting to recover local data...');
      var data = await retrieveDataFromDb(sharedPreferences, petitionId);
      if (data != null) {
        EasyLoading.showSuccess(successMessage);
        return data;
      } else {
        EasyLoading.showError(errMessage);
        if (throwError) {
          return createNetworkError(context, action, onPressed);
        }
        return null;
      }
    } else
      return true;
  }
}
