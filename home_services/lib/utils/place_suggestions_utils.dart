import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart';

class Place {
  String route;
  String city;
  String country;
  double lat;
  double lng;

  Place({
    this.route,
    this.city,
    this.country,
    this.lat,
    this.lng,
  });

  @override
  String toString() {
    return 'Place(street: $route, city: $city, country: $country, lat: $lat, lng: $lng)';
  }
}

class Suggestion {
  final String placeId;
  final String description;

  Suggestion(this.placeId, this.description);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId)';
  }
}

class PlaceApiProvider {
  final client = Client();

  PlaceApiProvider(this.sessionToken);

  final sessionToken;

  Future<List<Suggestion>> fetchSuggestions(String input, String lang) async {
    final request =
        Uri.https('maps.googleapis.com', 'maps/api/place/autocomplete/json', {
      "input": input,
      "types": "address",
      "language": lang,
      "components": "country:co",
      "key": env['GOOGLE_MAPS_API_KEY'],
      "sessiontoken": sessionToken
    });

    final response = await client.get(request);
    print(response.body);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        return result['predictions']
            .map<Suggestion>((p) => Suggestion(p['place_id'], p['description']))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<Place> getPlaceDetailFromId(String placeId) async {
    final request =
        Uri.https('maps.googleapis.com', 'maps/api/place/details/json', {
      "place_id": placeId,
      "fields": "address_component,geometry",
      "key": env['GOOGLE_MAPS_API_KEY'],
      "sessiontoken": sessionToken
    });
    final response = await client.get(request);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      print(result);
      if (result['status'] == 'OK') {
        final components =
            result['result']['address_components'] as List<dynamic>;
        // build result
        final place = Place();
        components.forEach((c) {
          final List type = c['types'];
          if (type.contains('route')) {
            place.route = c['long_name'];
          }
          if (type.contains('locality') ||
              type.contains('administrative_area_level_2')) {
            place.city = c['long_name'];
          }
          if (type.contains('country')) {
            place.country = c['long_name'];
          }
        });
        place.lat = result['result']['geometry']["location"]["lat"];
        place.lng = result['result']['geometry']["location"]["lng"];
        return place;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }
}
