import 'package:flutter/material.dart';
import 'package:home_services/ui/font_constants.dart';

abstract class FormUtils {
  List<Widget> createSpacedChildren(List<Widget> listOfWidgets) {
    for (var i = 0; i < listOfWidgets.length; i = i + 2) {
      listOfWidgets.insert(
          i + 1,
          SizedBox(
            height: 15,
          ));
    }
    return listOfWidgets;
  }

  TextFormField createFormInput(
      {@required String labelName,
      String hintText,
      String type,
      bool obscureText,
      IconData suffixIcon,
      Function onTap,
      TextInputType keyboardType,
      FocusNode focusNode,
      bool readOnly,
      Function validator,
      @required TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      onTap: onTap,
      readOnly: readOnly ?? false,
      validator: validator,
      keyboardType: keyboardType,
      focusNode: focusNode,
      autovalidateMode:
          validator != null ? AutovalidateMode.onUserInteraction : null,
      decoration: InputDecoration(
          labelText: labelName,
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
          ),
          filled: true,
          hintStyle: new TextStyle(color: Colors.grey[800]),
          hintText: hintText,
          suffixIcon: Icon(suffixIcon),
          fillColor: Colors.white70),
      style: FontConstants.displayStyle(
          fontSize: 20.0, height: 1.2, color: Colors.black),
      obscureText: obscureText == null ? false : obscureText,
    );
  }
}
