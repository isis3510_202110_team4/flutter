import 'dart:convert';

import 'package:crypto/crypto.dart';

bool checkIfEqual(localData, remoteData) {
  String currentData = localData.toString() ?? json.encode(localData);
  List<int> bytesCurrent = utf8.encode(currentData);
  String hashCurrent = sha256.convert(bytesCurrent).toString();
  String stringifiedData = remoteData.toString() ?? json.encode(remoteData);
  List<int> bytesRetrieved = utf8.encode(stringifiedData);
  String hashRetrieved = sha256.convert(bytesRetrieved).toString();
  return hashCurrent == hashRetrieved;
}
