library flutter_recaptcha_v2;

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;

final flutterWebviewPlugin = new FlutterWebviewPlugin();

class RecaptchaV2 extends StatefulWidget {
  final String apiKey;
  final String apiSecret;
  final String pluginURL;
  final RecaptchaV2Controller controller;
  bool visibleCancelBottom;
  String textCancelButtom;

  final ValueChanged<bool> onVerifiedSuccessfully;
  final ValueChanged<String> onVerifiedError;

  RecaptchaV2({
    this.apiKey,
    this.apiSecret,
    this.pluginURL: "https://recaptcha-flutter-plugin.firebaseapp.com/",
    this.visibleCancelBottom: false,
    this.textCancelButtom: "CANCEL CAPTCHA",
    RecaptchaV2Controller controller,
    this.onVerifiedSuccessfully,
    this.onVerifiedError,
  })  : controller = controller ?? RecaptchaV2Controller(),
        assert(apiKey != null, "Google ReCaptcha API KEY is missing."),
        assert(apiSecret != null, "Google ReCaptcha API SECRET is missing.");

  @override
  State<StatefulWidget> createState() => _RecaptchaV2State();
}

class _RecaptchaV2State extends State<RecaptchaV2> {
  RecaptchaV2Controller controller;

  void verifyToken(String token) async {
    String url = "https://www.google.com/recaptcha/api/siteverify";
    http.Response response = await http.post(Uri.parse(url), body: {
      "secret": widget.apiSecret,
      "response": token,
    });

    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      if (json['success']) {
        widget.onVerifiedSuccessfully(true);
        flutterWebviewPlugin.close();
      } else {
        widget.onVerifiedSuccessfully(false);
        widget.onVerifiedError(json['error-codes'].toString());
      }
    }

    // hide captcha
    controller.hide();
  }

  void onListen() {
    if (controller.visible) {
      if (flutterWebviewPlugin != null) {
        flutterWebviewPlugin.clearCache();
        flutterWebviewPlugin.reload();
      }
      flutterWebviewPlugin.onStateChanged.listen((viewState) async {
        if (viewState.type == WebViewState.finishLoad) {
          flutterWebviewPlugin.evalJavascript(
              'Array.from(document.querySelector("#html_element").children)[0].style.width ="340px";'
              'Array.from(document.querySelector("#html_element").children)[0].style.height="100px"; '
              'Array.from(document.querySelector("#html_element").children)[0].style.padding="70px 0";'
              'Array.from(document.querySelector("#html_element").children)[0].style["text-align"]="center";'
              'Array.from(document.querySelector("#html_element").children)[0].style.margin="auto";');
        }
      });
    }
    setState(() {
      controller.visible;
    });
  }

  @override
  void initState() {
    controller = widget.controller;
    controller.addListener(onListen);
    super.initState();
  }

  @override
  void didUpdateWidget(RecaptchaV2 oldWidget) {
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(onListen);
      controller = widget.controller;
      controller.removeListener(onListen);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.removeListener(onListen);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return controller.visible
        ? Stack(
            children: <Widget>[
              WebviewScaffold(
                url: "${widget.pluginURL}?api_key=${widget.apiKey}",
                javascriptChannels: <JavascriptChannel>[
                  JavascriptChannel(
                    name: 'RecaptchaFlutterChannel',
                    onMessageReceived: (JavascriptMessage receiver) {
                      String _token = receiver.message;
                      if (_token.contains("verify")) {
                        _token = _token.substring(7);
                      }
                      verifyToken(_token);
                    },
                  ),
                ].toSet(),
              ),
              Visibility(
                visible: widget.visibleCancelBottom,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: 60,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: TextButton(
                            child: Text(widget.textCancelButtom),
                            onPressed: () {
                              controller.hide();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        : Container();
  }
}

class RecaptchaV2Controller extends ChangeNotifier {
  bool isDisposed = false;
  List<VoidCallback> _listeners = [];

  bool _visible = false;
  bool get visible => _visible;

  void show() {
    _visible = true;
    if (!isDisposed) notifyListeners();
  }

  void hide() {
    _visible = false;
    if (!isDisposed) notifyListeners();
  }

  @override
  void dispose() {
    _listeners = [];
    isDisposed = true;
    super.dispose();
  }

  @override
  void addListener(listener) {
    _listeners.add(listener);
    super.addListener(listener);
  }

  @override
  void removeListener(listener) {
    _listeners.remove(listener);
    super.removeListener(listener);
  }
}
