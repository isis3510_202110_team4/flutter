import 'dart:convert';
import 'dart:developer';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:home_services/models/service_provider.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/utils/local_db_utils.dart';
import 'package:home_services/utils/network_utils.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ServiceProviderController with NetworkUtils {
  static Future<List<ServiceProvider>> fetchAllProviders(
      SharedPreferences sharedPreferences) async {
    List<ServiceProvider> providers = [];
    final response = await http.get(
        Uri.http(env['API_ENDPOINT'], '/api/users/worker/', {"format": "json"}),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }).timeout(Duration(seconds: 5), onTimeout: () {
      throw Exception('Failed to load service providers');
    });
    if (response.statusCode == 200) {
      var map = json.decode(utf8.decode(response.bodyBytes));
      storeAPIResponse(map, "fetchAllProviders", sharedPreferences);
      Iterable l = json.decode(utf8.decode(response.bodyBytes));
      List<ServiceProvider> list = List<ServiceProvider>.from(l.map((model) {
        return ServiceProvider.fromJson(model);
      }));
      providers = list;
    } else {
      log(response.body);
      throw Exception('Failed to load service providers');
    }
    return providers;
  }

  //Unused
  static Future<ServiceProvider> fetchOneProvider(
      SharedPreferences sharedPreferences, int providerId) async {
    final response = await http.get(
        Uri.http(env['API_ENDPOINT'], '/api/users/worker/detail/$providerId',
            {"format": "json"}),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }).timeout(Duration(seconds: 5), onTimeout: () {
      throw Exception('Failed to load service providers');
    });
    if (response.statusCode == 200) {
      var map = json.decode(utf8.decode(response.bodyBytes));
      storeAPIResponse(map, "fetchProvider$providerId", sharedPreferences);
      return ServiceProvider.fromJson(map);
    } else {
      log(response.body);
      throw Exception('Failed to load service providers');
    }
  }

  static Future<List<ServiceProvider>> fetchClosestProviders(
      SharedPreferences sharedPreferences, User userState) async {
    List<ServiceProvider> providers = [];
    if (userState.id == null || userState.id < 0) {
      throw Exception('Failed to load service providers');
    }
    if (providers.isEmpty) {
      final response = await http.get(
          Uri.http(env['API_ENDPOINT'],
              '/api/users/worker/closest/${userState.id}', {"format": "json"}),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          }).timeout(Duration(seconds: 5), onTimeout: () {
        throw Exception('Failed to load service providers');
      });
      final responseServices = await http.get(
          Uri.http(env['API_ENDPOINT'], '/api/service/', {"format": "json"}),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          }).timeout(Duration(seconds: 5), onTimeout: () {
        throw Exception('Failed to load service providers');
      });
      if (response.statusCode == 200 && responseServices.statusCode == 200) {
        var responseMap = json.decode(utf8.decode(response.bodyBytes));
        Iterable listProviders = responseMap;

        var responseServicesMap =
            json.decode(utf8.decode(responseServices.bodyBytes));

        List services = responseServicesMap;
        List objToStore = [];
        List<ServiceProvider> list =
            List<ServiceProvider>.from(listProviders.map((model) {
          var jobs = services
              .where((element) => element['worker'] == model['id'])
              .toList();
          if (jobs.length > 0) {
            model['Services Completed'] = jobs;
          }
          objToStore.add(model);
          return ServiceProvider.fromJson(model);
        }));
        storeAPIResponse(
            objToStore, "fetchClosestProviders", sharedPreferences);
        providers = list;
      } else {
        log(response.body);
        throw Exception('Failed to load service providers');
      }
    }
    return providers;
  }

  static Future<List<ServiceProvider>> fetchRecommendedProviders(
      SharedPreferences sharedPreferences, User userState) async {
    List<ServiceProvider> providers = [];
    if (userState.id == null || userState.id < 0) {
      throw Exception('Failed to load service providers');
    }
    if (providers.isEmpty) {
      final response = await http.get(
          Uri.http(
              env['API_ENDPOINT'],
              '/api/users/client/detail/${userState.id}/favorite_category_workers/',
              {"format": "json"}),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          }).timeout(Duration(seconds: 5), onTimeout: () {
        throw Exception('Failed to load service providers');
      });
      final responseServices = await http.get(
          Uri.http(env['API_ENDPOINT'], '/api/service/', {"format": "json"}),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          }).timeout(Duration(seconds: 5), onTimeout: () {
        throw Exception('Failed to load service providers');
      });
      if (response.statusCode == 200 && responseServices.statusCode == 200) {
        var responseMap = json.decode(utf8.decode(response.bodyBytes));
        Iterable listProviders = responseMap;

        var responseServicesMap =
            json.decode(utf8.decode(responseServices.bodyBytes));

        List services = responseServicesMap;
        List objToStore = [];
        List<ServiceProvider> list =
            List<ServiceProvider>.from(listProviders.map((model) {
          var jobs = services
              .where((element) => element['worker'] == model['id'])
              .toList();
          if (jobs.length > 0) {
            model['Services Completed'] = jobs;
          }
          objToStore.add(model);
          return ServiceProvider.fromJson(model);
        }));
        storeAPIResponse(jsonEncode(objToStore), "fetchRecommendedProviders",
            sharedPreferences);
        providers = list;
        print(
            "Recommended providers for user ${userState.id} --- ${providers}");
      } else {
        log(response.body);
        throw Exception('Failed to load service providers');
      }
    }
    return providers;
  }

  static Future<List<ServiceProvider>> fetchLocalClosestProviders(
      SharedPreferences sharedPreferences) async {
    var localData =
        await retrieveDataFromDb(sharedPreferences, "fetchClosestProviders");
    localData = localData as List;
    if (localData != null) {
      List<ServiceProvider> list =
          List<ServiceProvider>.from(localData.map((model) {
        return ServiceProvider.fromJson(model);
      }));
      return list;
    }
    return null;
  }

  static Future<List<ServiceProvider>> fetchLocalRecommendedProviders(
      SharedPreferences sharedPreferences) async {
    var localData = await retrieveDataFromDb(
        sharedPreferences, "fetchRecommendedProviders");
    localData = json.decode(localData) as Iterable;
    if (localData != null) {
      List<ServiceProvider> list =
          List<ServiceProvider>.from(localData.map((model) {
        return ServiceProvider.fromJson(model);
      }));
      return list;
    }
    return null;
  }
}
