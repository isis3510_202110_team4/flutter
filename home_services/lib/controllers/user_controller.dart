import 'dart:convert';
import 'dart:developer';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:home_services/controllers/service_provider_controller.dart';
import 'package:home_services/models/service.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/utils/local_db_utils.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart' show DateFormat;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/service_provider.dart';

class UserController {
  static Future<bool> postUser(User user) async {
    EasyLoading.show(status: 'Creating user ...');
    final uri = Uri.http(env['API_ENDPOINT'], 'api/users/register/');
    final response = await http.post(uri,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(user.toJson()));
    if (response.statusCode != 201) {
      log(response.body);
      EasyLoading.showError("Couldn't create user - ${response.body}");
      return false;
    }
    EasyLoading.dismiss();
    return true;
  }

  static Future<bool> postSurvey(
      int serviceProviderId, int userId, String feature) async {
    final uri = Uri.http(env['API_ENDPOINT'], 'api/survey/');
    Map<String, dynamic> obj = new Map();
    obj["client"] = userId;
    obj["worker"] = serviceProviderId;
    obj["options"] = feature;
    if (feature == null || feature.isEmpty) {
      EasyLoading.showError("Couldn't publish vote");
      return false;
    }
    final response = await http.post(uri,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(obj));
    if (response.statusCode != 201) {
      log(response.body);
      EasyLoading.showError("Couldn't publish vote");
      return false;
    }
    EasyLoading.showSuccess("Voted successfully!");
    return true;
  }

  static Future<bool> createRequest(
      ServiceProvider serviceProvider,
      SharedPreferences sharedPreferences,
      User userState,
      String source,
      DateTime date,
      double address_lat,
      double address_lon) async {
    EasyLoading.show(status: 'Booking immediate service  ...');

    final uriService = Uri.http(env['API_ENDPOINT'], 'api/service/');

    print("Date ${DateFormat('EEEE, HH:mm').format(date)}");
    final Map<String, dynamic> serviceToSend = {
      "client": userState.id,
      "worker": serviceProvider.id,
      "category": serviceProvider.category[0] ?? "Plumbing",
      "desc": DateFormat('EEEE, HH:mm').format(date),
      "date_scheduled": date.toIso8601String(),
      "address_lat": address_lat.toStringAsFixed(6),
      "address_lon": address_lon.toStringAsFixed(6),
      "source": source,
      "payment_method": "CH"
    };
    print("Service to send $serviceToSend");
    final response = await http.post(uriService,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(serviceToSend));
    if (response.statusCode <= 300) {
      final uri = Uri.http(env['API_ENDPOINT'], 'api/users/messages/');
      final Map<String, dynamic> bodyToSend = {
        'number': serviceProvider.phone_number != null
            ? serviceProvider.phone_number
            : '+573002463734',
        'message': 'A user booked an immediate service!'
      };
      http.post(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(bodyToSend));

      EasyLoading.dismiss();
      EasyLoading.showSuccess(
          "${serviceProvider.category[0][0].toUpperCase()}${serviceProvider.category[0].substring(1)} service with ${serviceProvider.name} booked correctly");
      return true;
    } else {
      EasyLoading.showError(
          "Couldn't book immediate service - ${response.statusCode}");
      return false;
    }
  }

  static Future<User> getUserByEmail(
      String email, SharedPreferences sharedPreferences) async {
    final uriList = Uri.http(env['API_ENDPOINT'], 'api/users/client/');
    final responseList = await http.get(uriList, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    var map = json.decode(utf8.decode(responseList.bodyBytes));
    storeAPIResponse(map, "fetchAllUsers", sharedPreferences);
    List<User> list = List<User>.from(map.map((model) {
      return User.fromJson(model);
    }));
    print("id -> $email");
    User loggedUser = list.firstWhere((listUser) => listUser.email == email,
        orElse: () => null);
    return loggedUser;
  }

  static Future<User> getCurrentUser(
      SharedPreferences sharedPreferences) async {
    final token = sharedPreferences.get("X-TOKEN");
    final uriList = Uri.http(env['API_ENDPOINT'], 'api/users/current-user/');
    try {
      final responseList = await http.get(uriList, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Token $token'
      });

      var map = json.decode(utf8.decode(responseList.bodyBytes));
      storeAPIResponse(map, "currentUserDetails", sharedPreferences);
      if (responseList.statusCode < 300) {
        return User.fromJson(map);
      }
    } catch (err) {
      print("Err $err");
      return User.fromJson(
          await retrieveDataFromDb(sharedPreferences, "currentUserDetails"));
    }
    print("Post err");
    return User.fromJson(
        await retrieveDataFromDb(sharedPreferences, "currentUserDetails"));
  }

  static Future<List<Service>> getServices(
      SharedPreferences sharedPreferences, User userState) async {
    final uri = Uri.http(env['API_ENDPOINT'],
        '/api/users/client/detail/${userState.id}/services/');
    try {
      final response = await http.get(uri, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      });
      List<Service> services = [];
      if (response.statusCode == 200) {
        var map = json.decode(utf8.decode(response.bodyBytes));

        Iterable l = json.decode(utf8.decode(response.bodyBytes));
        List<ServiceProvider> serviceProviders =
            await ServiceProviderController.fetchAllProviders(
                sharedPreferences);
        List<Service> list = List<Service>.from(l.map((model) {
          var serviceWorker = serviceProviders
              .firstWhere((element) => element.id == model['worker']);
          if (serviceWorker != null) {
            model['workerName'] = serviceWorker.name;
          }
          return Service.fromJson(model);
        }));
        storeAPIResponse(json.encode(list), "getServices", sharedPreferences);
        services = list;
      } else {
        log(response.body);
        throw Exception('Failed to load service providers');
      }
      return services;
    } catch (err) {
      return [];
    }
  }

  static Future<bool> addToFavorites(
      int userId, int workerId, String workerName) async {
    final uri = Uri.http(env['API_ENDPOINT'],
        '/api/users/client/detail/$userId/add_favorite/$workerId/');
    final response = await http.post(uri, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    if (response.statusCode < 300) {
      EasyLoading.dismiss();
      EasyLoading.showSuccess("$workerName added to favorites successfully");
      return true;
    }
    return false;
  }

  static Future<bool> removeFromFavorites(
      int userId, int workerId, String workerName) async {
    final uri = Uri.http(env['API_ENDPOINT'],
        '/api/users/client/detail/$userId/delete_favorite/$workerId/');
    final response = await http.post(uri, headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    });
    if (response.statusCode < 300) {
      EasyLoading.dismiss();
      EasyLoading.showSuccess("$workerName removed to favorites successfully");
      return true;
    }
    return false;
  }

  static Future<User> login(
      SharedPreferences sharedPreferences, User user) async {
    EasyLoading.show(status: 'Signing in ...');
    final jsonHeader = new Map<String, String>();
    jsonHeader['Content-Type'] = 'application/json';
    final uri = Uri.http(env['API_ENDPOINT'], 'api/users/login/');
    try {
      final response = await http.post(uri,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(user.toJson()));
      if (response.statusCode >= 400) {
        var map = json.decode(utf8.decode(response.bodyBytes));
        EasyLoading.showError(
            "${map != null && map["non_field_errors"] != null && map["non_field_errors"][0] != null ? map["non_field_errors"][0].toString() : "Couldn't log in"}");
        return null;
      }
      var map = json.decode(utf8.decode(response.bodyBytes));
      EasyLoading.dismiss();
      print("X-TOKEN ${map["token"].toString()}");
      sharedPreferences.setString("X-TOKEN", map["token"].toString());
      User loggedUser = await getCurrentUser(sharedPreferences);
      storeToLocalDb(loggedUser.toJson(), "currentUser");
      return loggedUser;
    } catch (err) {
      return null;
    }
  }
}
