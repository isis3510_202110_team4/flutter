import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FontConstants {
  static const appBarTextStyle = GoogleFonts.poppins;
  static const displayStyle = GoogleFonts.sourceSansPro;

  static Text formatAppBar({String text, Color color, double fontSize}) {
    final appBarTextStyle = GoogleFonts.poppins;
    return Text(text,
        style: appBarTextStyle(
            fontWeight: FontWeight.w300,
            color: color,
            fontSize: fontSize == null ? 18.0 : fontSize));
  }

  static Text formatDisplayText(
      {@required String text,
      Color color,
      double fontSize,
      FontWeight fontWeight,
      double letterSpacing}) {
    final displayTextStyle = GoogleFonts.sourceSansPro;
    return Text(text,
        style: displayTextStyle(
            fontWeight: fontWeight,
            color: color,
            fontSize: fontSize,
            letterSpacing: letterSpacing == null ? 0 : letterSpacing));
  }
}
