class Service {
  final int id;
  final int client;
  final int worker;
  final String workerName;
  final String category;
  final String address_lon;
  final String address_lat;
  final int rating;
  final bool in_progress;
  final String desc;
  final DateTime date_scheduled;
  final DateTime date_finished;
  final double price;
  final bool cancelled;
  final String payment_method;

  Service({
    this.id,
    this.client,
    this.worker,
    this.workerName,
    this.category,
    this.address_lon,
    this.address_lat,
    this.rating,
    this.in_progress,
    this.desc,
    this.date_scheduled,
    this.date_finished,
    this.price,
    this.cancelled,
    this.payment_method,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'client': this.client,
      'worker': this.worker,
      'category': this.category,
      'address_lon': this.address_lon,
      'address_lat': this.address_lat,
      'rating': this.rating,
      'in_progress': this.in_progress,
      'desc': this.desc,
      'date_scheduled': this.date_scheduled != null
          ? '${this.date_scheduled.year}-${this.date_scheduled.month}-${this.date_scheduled.day}'
          : '',
      'date_finished': this.date_finished != null
          ? '${this.date_finished.year}-${this.date_finished.month}-${this.date_finished.day}'
          : '',
      'price': this.price,
      'cancelled': this.cancelled,
      'payment_method': this.payment_method,
    };
  }

  factory Service.fromJson(Map<String, dynamic> json) {
    var retrievedDateScheduled;
    var retrievedDateFinished;
    if (json['date_scheduled'].toString().isNotEmpty) {
      try {
        retrievedDateScheduled = DateTime.parse(json['date_scheduled']);
      } catch (err) {
        print(err);
      }
    }
    if (json['date_finished'].toString().isNotEmpty) {
      try {
        retrievedDateFinished = DateTime.parse(json['date_finished']);
      } catch (err) {
        print(err);
      }
    }
    return Service(
      id: json["id"],
      client: json["client"],
      worker: json["worker"],
      workerName: json["workerName"],
      category: json["category"],
      address_lon: json["address_lon"],
      address_lat: json["address_lat"],
      rating: json["rating"] ?? 0,
      in_progress: json["in_progress"] ?? false,
      desc: json["desc"],
      date_scheduled: retrievedDateScheduled,
      date_finished: retrievedDateFinished,
      price: json["price"],
      cancelled: json["cancelled"],
      payment_method: json["payment_method"],
    );
  }

  @override
  String toString() {
    return 'Appointment(category: $category, id: $id, desc: $desc, date: $date_scheduled, client: $client, worker: $worker)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Service && o.id == id;
  }

  @override
  int get hashCode {
    return id.hashCode;
  }
}
