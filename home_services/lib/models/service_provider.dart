import 'dart:math';

import 'package:home_services/models/base_user.dart';
import 'package:home_services/models/job.dart';

class ServiceProvider extends BaseUser {
  String rating;
  int service_radius;
  String price_per_hour;
  List category;
  List skill;
  String profile_picture;
  List previous_jobs_img;
  String bio;
  List previousJobs;
  ServiceProvider(
      {String name,
      String username,
      String password,
      String email,
      String id_number,
      String address_desc,
      double address_lon,
      double address_lat,
      String phone_number,
      int id,
      bool is_worker,
      DateTime birth_date,
      this.rating,
      this.service_radius,
      this.price_per_hour,
      this.category,
      this.skill,
      this.profile_picture,
      this.previous_jobs_img,
      this.bio,
      this.previousJobs})
      : super(
            name: name,
            username: username,
            password: password,
            email: email,
            id_number: id_number,
            address_desc: address_desc,
            address_lon: address_lon,
            address_lat: address_lat,
            phone_number: phone_number,
            id: id,
            is_worker: true,
            birth_date: birth_date);

  factory ServiceProvider.fromJson(Map<String, dynamic> json) {
    var birthDate;
    var retrievedDate;
    if (json['birth_date'].toString().isNotEmpty) {
      try {
        birthDate = json['birth_date'].split("-");
        retrievedDate = DateTime(int.parse(birthDate[0]),
            int.parse(birthDate[1]), int.parse(birthDate[2]));
      } catch (err) {
        print(err);
      }
    }
    List prevJobs = json['previous_jobs_img'];
    List servicesCompleted = json['Services Completed'];
    List<Job> retrievedJobs = [];
    var currentService;
    if (prevJobs != null &&
        prevJobs.length > 0 &&
        servicesCompleted != null &&
        servicesCompleted.length > 0) {
      for (var i = 0; i < min(prevJobs.length, servicesCompleted.length); i++) {
        currentService = servicesCompleted[i];
        if (currentService["in_progress"] != null &&
            currentService["in_progress"] == false) {
          retrievedJobs.add(Job(
              category: currentService['category'],
              desc: currentService['desc'],
              imageUrl: prevJobs[i]));
        }
      }
    }

    return ServiceProvider(
        name: json['name'],
        address_desc: json['address_desc'],
        username: json['username'],
        email: json['email'],
        id_number: json['id_number'],
        address_lon: json['address_lon'],
        address_lat: json['address_lat'],
        phone_number: json['phone_number'],
        id: json['id'],
        is_worker: json['is_worker'],
        rating: json['rating'],
        service_radius: json['service_radius'],
        price_per_hour: json['price_per_hour'],
        category: json['category'],
        skill: json['skill'],
        profile_picture: json['profile_picture'],
        bio: json['bio'],
        previous_jobs_img: json['previous_jobs_img'],
        previousJobs: retrievedJobs,
        birth_date: retrievedDate);
  }

  toString() {
    return 'ServiceProvider(name: $name, username: $username, email: $email, id: $id, previous_jobs: $previousJobs, bio: ${bio?.substring(0, bio.length > 50 ? 50 : bio.length - 1)})';
  }

  Map<String, dynamic> toJson() {
    final baseMap = super.toJson();
    baseMap["rating"] = rating;
    baseMap["service_radius"] = service_radius;
    baseMap["price_per_hour"] = price_per_hour;
    baseMap["category"] = category ?? [];
    baseMap["skills"] = skill ?? [];
    baseMap["profile_picture"] = profile_picture;
    baseMap["previous_jobs_img"] = previous_jobs_img ?? [];
    return baseMap;
  }

  bool contains(String value) {
    return name.toLowerCase().contains(value.toLowerCase()) ||
        '$rating'.contains(value) ||
        '$service_radius'.contains(value) ||
        '$price_per_hour'.contains(value) ||
        '$category'.contains(value) ||
        '$skill'.contains(value);
  }

  void copyWithin(ServiceProvider serviceProvider) {
    name = serviceProvider.name ?? this.name;
    username = serviceProvider.username ?? this.username;
    password = serviceProvider.password ?? this.password;
    email = serviceProvider.email ?? this.email;
    id_number = serviceProvider.id_number ?? this.id_number;
    address_desc = serviceProvider.address_desc ?? this.address_desc;
    address_lon = serviceProvider.address_lon ?? this.address_lon;
    address_lat = serviceProvider.address_lat ?? this.address_lat;
    phone_number = serviceProvider.phone_number ?? this.phone_number;
    id = serviceProvider.id ?? this.id;
    is_worker = serviceProvider.is_worker ?? this.is_worker;
    birth_date = serviceProvider.birth_date ?? this.birth_date;
    rating = serviceProvider.rating ?? this.rating;
    service_radius = serviceProvider.service_radius ?? this.service_radius;
    price_per_hour = serviceProvider.price_per_hour ?? this.price_per_hour;
    category = serviceProvider.category ?? this.category;
    skill = serviceProvider.skill ?? this.skill;
    profile_picture = serviceProvider.profile_picture ?? this.profile_picture;
    previous_jobs_img =
        serviceProvider.previous_jobs_img ?? this.previous_jobs_img;
  }

  int compareTo(ServiceProvider another, String attribute, [String order]) {
    if (order == "Descending") {
      return -this.get(attribute).compareTo(another.get(attribute));
    }
    return this.get(attribute).compareTo(another.get(attribute));
  }

  static getCategory(String identifier) {
    switch (identifier) {
      case 'PB':
        return 'Plumbing';
      case 'HK':
        return 'Housekeeping';
      case 'ET':
        return 'Electricity';
      case 'CP':
        return 'Carpentry';
      case 'LS':
        return 'Locksmith';
      default:
        return '';
    }
  }
}
