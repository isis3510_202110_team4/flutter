class Job {
  final String category;
  final String desc;
  final String imageUrl;

  Job({this.category, this.desc, this.imageUrl});

  Map<String, dynamic> toJson() {
    return {
      'category': this.category,
      'desc': this.desc,
      'imageUrl': this.imageUrl,
    };
  }

  factory Job.fromJson(Map<String, dynamic> json) {
    return Job(
      category: json["category"],
      desc: json["desc"],
      imageUrl: json["imageUrl"],
    );
  }

  @override
  String toString() {
    return 'Job(category: $category, desc: $desc)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Job && o.category == category && o.desc == desc;
  }

  @override
  int get hashCode {
    return desc.hashCode ^ category.hashCode;
  }
}
