class BaseUser {
  String name;
  String username;
  String password;
  String email;
  String id_number;
  String address_desc;
  double address_lon;
  double address_lat;
  String phone_number;
  int id;
  bool is_worker;
  DateTime birth_date;
  BaseUser(
      {this.name,
      this.username,
      this.password,
      this.email,
      this.id_number,
      this.address_desc,
      this.address_lon,
      this.address_lat,
      this.phone_number,
      this.id,
      this.is_worker,
      this.birth_date});

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'username': username,
      'password': password,
      'email': email,
      'id_number': id_number,
      'address_desc': address_desc,
      'address_lon': address_lon,
      'address_lat': address_lat,
      'phone_number': phone_number,
      'id': id,
      'is_worker': is_worker,
      'birth_date': birth_date != null
          ? '${birth_date.year}-${birth_date.month}-${birth_date.day}'
          : ''
    };
  }

  dynamic get(String propertyName) {
    var _mapRep = this.toJson();
    print(_mapRep);
    if (_mapRep.containsKey(propertyName)) {
      return _mapRep[propertyName];
    }
    throw ArgumentError('property not found');
  }

  factory BaseUser.fromJson(Map<String, dynamic> json) {
    return BaseUser(
        name: json['name'],
        address_desc: json['address_desc'],
        username: json['username'],
        email: json['email'],
        id_number: json['id_number'],
        address_lon: json['address_lon'],
        address_lat: json['address_lat'],
        phone_number: json['phone_number'],
        id: json['id'],
        is_worker: json['is_worker'],
        birth_date: json['birth_date']);
  }
}
