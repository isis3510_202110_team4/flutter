import 'package:home_services/models/base_user.dart';

class User extends BaseUser {
  List favorites = [];
  User(
      {String name,
      String username,
      String password,
      String email,
      String id_number,
      String address_desc,
      double address_lon,
      double address_lat,
      String phone_number,
      int id,
      bool is_worker,
      DateTime birth_date,
      this.favorites})
      : super(
            name: name,
            username: username,
            password: password,
            email: email,
            id_number: id_number,
            address_desc: address_desc,
            address_lon: address_lon,
            address_lat: address_lat,
            phone_number: phone_number,
            id: id,
            is_worker: false,
            birth_date: birth_date);

  factory User.fromJson(Map<String, dynamic> json) {
    var birthDate;
    var retrievedDate;
    if (json['birth_date'].toString().isNotEmpty) {
      try {
        birthDate = json['birth_date'].split("-");
        retrievedDate = DateTime(int.parse(birthDate[0]),
            int.parse(birthDate[1]), int.parse(birthDate[2]));
      } catch (err) {
        print(err);
      }
    }

    return User(
        name: json['name'],
        address_desc: json['address_desc'],
        username: json['username'],
        password: json['password'],
        email: json['email'],
        id_number: json['id_number'],
        address_lon: json['address_lon'] is String
            ? double.parse(json['address_lon'])
            : json['address_lon'],
        address_lat: json['address_lat'] is String
            ? double.parse(json['address_lat'])
            : json['address_lat'],
        phone_number: json['phone_number'],
        id: json['id'],
        is_worker: json['is_worker'],
        favorites: json['favorites'],
        birth_date: retrievedDate);
  }

  Map<String, dynamic> toJson() {
    final baseMap = super.toJson();
    if (favorites != null) {
      baseMap["favorites"] = favorites;
    }
    baseMap["favorites"] = [];
    return baseMap;
  }

  void copyWithin(User user) {
    name = user.name ?? this.name;
    username = user.username ?? this.username;
    password = user.password ?? this.password;
    email = user.email ?? this.email;
    id_number = user.id_number ?? this.id_number;
    address_desc = user.address_desc ?? this.address_desc;
    address_lon = user.address_lon ?? this.address_lon;
    address_lat = user.address_lat ?? this.address_lat;
    phone_number = user.phone_number ?? this.phone_number;
    id = user.id ?? this.id;
    is_worker = user.is_worker ?? this.is_worker;
    birth_date = user.birth_date ?? this.birth_date;
    favorites = user.favorites != null && user.favorites.length > 0
        ? user.favorites
        : this.favorites;
  }

  String toString() {
    return 'User(name: $name, username: $username, email: $email, id: $id)';
  }

  bool isNewUser() {
    return this.id == null || this.id < 0;
  }
}
