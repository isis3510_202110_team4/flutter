import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:home_services/models/service_provider.dart';
import 'package:home_services/models/user.dart';
import 'package:home_services/pages/navigation_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

final userState = StateProvider<User>((ref) {
  return;
});

final serviceProviderState = StateProvider<ServiceProvider>((ref) {
  return;
});

final listOfServiceProvidersState = StateProvider<List<ServiceProvider>>((ref) {
  return;
});

void main() async {
  await DotEnv.load(fileName: ".env");
  final sharedPreferences = await SharedPreferences.getInstance();
  runApp(ProviderScope(
    child: MyApp(sharedPreferences),
  ));
}

class MyApp extends StatelessWidget {
  final SharedPreferences sharedPreferences;

  const MyApp(this.sharedPreferences, {Key key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Home Services',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        backgroundColor: Colors.white,
      ),
      home: NavigationPage(sharedPreferences),
      builder: EasyLoading.init(),
    );
  }
}
