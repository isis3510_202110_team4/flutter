# Home Services

Flutter App to schedule home services.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Demo Images
<table>
  <tr>
    <td> <img src="assets/Register.jpg"  alt="Register" width=270px height=420px ></td>
    <td> <img src="assets/Login.jpg"  alt="Login" width=270px height=420px ></td>
    <td> <img src="assets/Map.jpg"  alt="Map" width=270px height=420px ></td>
    <td> <img src="assets/Search.jpg"  alt="Search" width=270px height=420px ></td>
   </tr> 
   <tr>
    <td> <img src="assets/Detail0.jpg"  alt="Detail0" width=270px height=420px ></td>
    <td> <img src="assets/Detail1.jpg"  alt="Detail1" width=270px height=420px ></td>
    <td> <img src="assets/Calendar.jpg"  alt="Calendar" width=270px height=420px ></td>
    <td> <img src="assets/Survey.jpg"  alt="Survey" width=270px height=420px ></td>
  </td>
  </tr>
</table>

## Download the application

Download APK: [APK](assets/INSTALL_ME.apk)

### Prerequisites

All the listed prerrequisites must be installed in order to use the application.

* [Flutter](https://flutter.dev/docs/get-started/install)
* [Dart](https://dart.dev/get-dart)
* [Android SDK](https://developer.android.com/studio/releases/platform-tools)
* Recommended but not neccesary: [Android Studio](https://developer.android.com/studio)


### Installing

* Clone repository using git

```bash
git clone https://gitlab.com/isis3510_202110_team4/flutter.git
```

* Install dependencies from pubspec.yaml
```bash
cd /home_services/
pub get
```


### Make dependencies

* Get a Google Maps API Key from [Cloud Console](https://console.cloud.google.com/google/maps-apis/overview).
* Get a Google Captcha Site Key and Secret Key from [Google Captcha](https://www.google.com/recaptcha/admin/create).

    * Set domain to `recaptcha-flutter-plugin.firebaseapp.com/` [GitHub Repo](https://github.com/mrgoonie/flutter_recaptcha_v2).

* Create a .env file at `home_services` path with the following properties:

```bash
API_ENDPOINT=<API_ENDPOINT>
GOOGLE_MAPS_API_KEY=<GOOGLE_MAPS_API_KEY>
GOOGLE_CAPTCHA_SITE_KEY=<GOOGLE_CAPTCHA_SITE_KEY>
GOOGLE_CAPTCHA_SECRET_KEY=<GOOGLE_CAPTCHA_SITE_KEY>
```

* Add Google Maps API Key AndroidManifest.xml in `home_services/android/app/src/main`

```xml
<meta-data android:name="com.google.android.geo.API_KEY"
        android:value="<GOOGLE_MAPS_API_KEY>"/>
```




### Running the application

* Run the application using VSCode or Android Studio



## Authors

* **Juan Sebastian Bravo Castelo** - [GitLab](https://gitlab.com/jsbravoc)
* **Michel Succar** - [GitLab](https://gitlab.com/micsucmec)
* **Andrés Silva** - [GitLab](https://gitlab.com/andresilva19971)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

